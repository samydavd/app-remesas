(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["app/Sistema/Administradores/Permisos"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/app/Sistema/Administradores/Permisos.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/app/Sistema/Administradores/Permisos.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      error: [],
      success: [],
      url: {
        current: this.$root.base_url + this.$route.path,
        permisos: {}
      },
      selects: {
        estados: [{
          label: 'Activo',
          value: 1
        }, {
          label: 'Inactivo',
          value: 0
        }]
      },
      filters: {
        id: null,
        nombre: null,
        estado: null
      },
      secciones: [],
      admin: {},
      table: {
        filtrando: false,
        showActions: true
      }
    };
  },
  created: function () {
    var _created = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              this.filtrar(); //this.permisos = await this.$root.permissions('/');

            case 1:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, this);
    }));

    function created() {
      return _created.apply(this, arguments);
    }

    return created;
  }(),
  methods: {
    limpiarMensajes: function limpiarMensajes() {
      this.success = [];
      this.error = [];
    },
    limpiar: function limpiar() {
      this.filters = {
        id: null,
        nombre: null,
        estado: null
      };
      this.filtrar(1);
    },
    filtrar: function filtrar() {
      var _this = this;

      this.limpiarMensajes();
      this.table.filtrando = true;
      var load = loading(this);
      var request = new FormData();
      this.filters.id && request.append('id', this.filters.id);
      this.filters.nombre && request.append('nombre', this.filters.nombre);
      this.filters.estado && request.append('estado', this.filters.estado.value);
      axios.post(this.url.current, request).then(function (response) {
        _this.secciones = response.data.secciones;
        _this.admin = response.data.admin;
      })["catch"](function (error) {
        _this.error = _this.$root.arrayResponse(error);
      })["finally"](function () {
        load.hide();
        _this.table.filtrando = false;
      });
    },
    guardar: function guardar() {
      var _this2 = this;

      this.limpiarMensajes();
      var load = loading(this);
      var permisos = [];
      this.secciones.map(function (seccion) {
        seccion._menus.map(function (menu) {
          menu._acciones.map(function (accion) {
            if (accion.check) {
              permisos.push(accion.id);
            }
          });
        });
      });
      axios.post(this.url.current + '/guardar', {
        permisos: permisos
      }).then(function (response) {
        _this2.success = response.data.success;
      })["catch"](function (error) {
        _this2.error = _this2.$root.arrayResponse(error);
      })["finally"](function () {
        load.hide();
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/app/Sistema/Administradores/Permisos.vue?vue&type=template&id=5a929dbb&":
/*!****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/app/Sistema/Administradores/Permisos.vue?vue&type=template&id=5a929dbb& ***!
  \****************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("alertas", { attrs: { success: _vm.success, error: _vm.error } }),
      _vm._v(" "),
      _c(
        "panel",
        { attrs: { type: "list" } },
        [
          _c("template", { slot: "header" }, [
            _vm._v("\n\t\t\t\tListado de acciones | \n\t\t\t\t"),
            Object.keys(_vm.admin).length > 0
              ? _c("span", [
                  _vm._v(
                    "\n\t\t\t\t\t" +
                      _vm._s(_vm.admin.nombre_completo) +
                      "\n\t\t\t\t"
                  )
                ])
              : _vm._e()
          ]),
          _vm._v(" "),
          _c("template", { slot: "buttons" }, [
            _c("div", { staticClass: "d-flex" }, [
              _c("div", { staticClass: "input-group mr-2" }, [
                _c("input", {
                  staticClass: "form-control",
                  attrs: { type: "text", placeholder: "Buscar" }
                }),
                _vm._v(" "),
                _c("div", { staticClass: "input-group-prepend" }, [
                  _c(
                    "div",
                    {
                      staticClass: "input-group-text",
                      attrs: { id: "btnGroupAddon" }
                    },
                    [_c("i", { staticClass: "fa fa-search" })]
                  )
                ])
              ]),
              _vm._v(" "),
              _c(
                "button",
                {
                  staticClass: "btn btn-success btn-sm",
                  on: {
                    click: function($event) {
                      return _vm.guardar()
                    }
                  }
                },
                [_vm._v("Guardar")]
              )
            ])
          ]),
          _vm._v(" "),
          _c("template", { slot: "main" }, [
            _c(
              "div",
              { staticClass: "w-100" },
              [
                _vm.table.filtrando
                  ? [
                      _c("div", { staticClass: "accordion" }, [
                        _c("div", { staticClass: "card bg-dark text-white" }, [
                          _c(
                            "div",
                            {
                              staticClass:
                                "card-header bg-dark-darker pointer-cursor d-flex align-items-center collapsed"
                            },
                            [
                              _vm._v(
                                "\n\t\t\t\t\t\t\t\t\tBuscando datos\n\t\t\t\t\t\t\t\t"
                              )
                            ]
                          )
                        ])
                      ])
                    ]
                  : !_vm.table.filtrando && _vm.secciones.length > 0
                  ? [
                      _c(
                        "div",
                        {
                          staticClass: "accordion",
                          attrs: { id: "accordion_seccion" }
                        },
                        [
                          _vm._l(_vm.secciones, function(seccion) {
                            return [
                              _c(
                                "div",
                                { staticClass: "card bg-dark text-white" },
                                [
                                  _c(
                                    "div",
                                    {
                                      staticClass:
                                        "card-header bg-dark-darker pointer-cursor d-flex align-items-center collapsed",
                                      attrs: {
                                        "data-toggle": "collapse",
                                        "data-target":
                                          "#collapse_s" + seccion.id,
                                        "aria-expanded": "false"
                                      }
                                    },
                                    [
                                      _vm._v(
                                        "\n\t\t\t\t\t\t\t\t\t\t" +
                                          _vm._s(
                                            _vm._f("ucwords")(seccion.nombre)
                                          ) +
                                          "\n\t\t\t\t\t\t\t\t\t"
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "div",
                                    {
                                      staticClass: "collapse",
                                      attrs: {
                                        id: "collapse_s" + seccion.id,
                                        "data-parent": "#accordion_seccion"
                                      }
                                    },
                                    [
                                      _c("div", { staticClass: "card-body" }, [
                                        seccion._menus.length > 0
                                          ? _c(
                                              "div",
                                              {
                                                staticClass: "accordion",
                                                attrs: { id: "accordion_menu" }
                                              },
                                              [
                                                _vm._l(seccion._menus, function(
                                                  menu
                                                ) {
                                                  return [
                                                    _c(
                                                      "div",
                                                      {
                                                        staticClass:
                                                          "card bg-white text-black"
                                                      },
                                                      [
                                                        _c(
                                                          "div",
                                                          {
                                                            staticClass:
                                                              "card-header bg-white-lighter pointer-cursor d-flex align-items-center collapsed",
                                                            attrs: {
                                                              "data-toggle":
                                                                "collapse",
                                                              "data-target":
                                                                "#collapse_m" +
                                                                menu.id,
                                                              "aria-expanded":
                                                                "false"
                                                            }
                                                          },
                                                          [
                                                            _c("i", {
                                                              staticClass:
                                                                "fa-fw mr-2 text-blue",
                                                              class: menu.icono
                                                            }),
                                                            _vm._v(
                                                              "\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t" +
                                                                _vm._s(
                                                                  _vm._f(
                                                                    "capitalize"
                                                                  )(menu.nombre)
                                                                ) +
                                                                "\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t"
                                                            )
                                                          ]
                                                        ),
                                                        _vm._v(" "),
                                                        _c(
                                                          "div",
                                                          {
                                                            staticClass:
                                                              "collapse",
                                                            attrs: {
                                                              id:
                                                                "collapse_m" +
                                                                menu.id,
                                                              "data-parent":
                                                                "#accordion_menu"
                                                            }
                                                          },
                                                          [
                                                            menu._acciones
                                                              .length > 0
                                                              ? [
                                                                  _vm._l(
                                                                    menu._acciones,
                                                                    function(
                                                                      accion
                                                                    ) {
                                                                      return [
                                                                        _c(
                                                                          "div",
                                                                          {
                                                                            staticClass:
                                                                              "accordion"
                                                                          },
                                                                          [
                                                                            _c(
                                                                              "div",
                                                                              {
                                                                                staticClass:
                                                                                  "card"
                                                                              },
                                                                              [
                                                                                _c(
                                                                                  "div",
                                                                                  {
                                                                                    staticClass:
                                                                                      " card-header pointer-cursor clearfix"
                                                                                  },
                                                                                  [
                                                                                    _c(
                                                                                      "span",
                                                                                      {
                                                                                        staticClass:
                                                                                          "float-left"
                                                                                      },
                                                                                      [
                                                                                        _vm._v(
                                                                                          "\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t" +
                                                                                            _vm._s(
                                                                                              _vm._f(
                                                                                                "capitalize"
                                                                                              )(
                                                                                                accion.nombre
                                                                                              )
                                                                                            ) +
                                                                                            "\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"
                                                                                        )
                                                                                      ]
                                                                                    ),
                                                                                    _vm._v(
                                                                                      " "
                                                                                    ),
                                                                                    _c(
                                                                                      "span",
                                                                                      {
                                                                                        staticClass:
                                                                                          "float-right"
                                                                                      },
                                                                                      [
                                                                                        _c(
                                                                                          "span",
                                                                                          {
                                                                                            staticClass:
                                                                                              "checkbox checkbox-css"
                                                                                          },
                                                                                          [
                                                                                            _c(
                                                                                              "input",
                                                                                              {
                                                                                                directives: [
                                                                                                  {
                                                                                                    name:
                                                                                                      "model",
                                                                                                    rawName:
                                                                                                      "v-model",
                                                                                                    value:
                                                                                                      accion.check,
                                                                                                    expression:
                                                                                                      "accion.check"
                                                                                                  }
                                                                                                ],
                                                                                                attrs: {
                                                                                                  type:
                                                                                                    "checkbox",
                                                                                                  id:
                                                                                                    "cssCheckbox" +
                                                                                                    accion.id
                                                                                                },
                                                                                                domProps: {
                                                                                                  checked: Array.isArray(
                                                                                                    accion.check
                                                                                                  )
                                                                                                    ? _vm._i(
                                                                                                        accion.check,
                                                                                                        null
                                                                                                      ) >
                                                                                                      -1
                                                                                                    : accion.check
                                                                                                },
                                                                                                on: {
                                                                                                  change: function(
                                                                                                    $event
                                                                                                  ) {
                                                                                                    var $$a =
                                                                                                        accion.check,
                                                                                                      $$el =
                                                                                                        $event.target,
                                                                                                      $$c = $$el.checked
                                                                                                        ? true
                                                                                                        : false
                                                                                                    if (
                                                                                                      Array.isArray(
                                                                                                        $$a
                                                                                                      )
                                                                                                    ) {
                                                                                                      var $$v = null,
                                                                                                        $$i = _vm._i(
                                                                                                          $$a,
                                                                                                          $$v
                                                                                                        )
                                                                                                      if (
                                                                                                        $$el.checked
                                                                                                      ) {
                                                                                                        $$i <
                                                                                                          0 &&
                                                                                                          _vm.$set(
                                                                                                            accion,
                                                                                                            "check",
                                                                                                            $$a.concat(
                                                                                                              [
                                                                                                                $$v
                                                                                                              ]
                                                                                                            )
                                                                                                          )
                                                                                                      } else {
                                                                                                        $$i >
                                                                                                          -1 &&
                                                                                                          _vm.$set(
                                                                                                            accion,
                                                                                                            "check",
                                                                                                            $$a
                                                                                                              .slice(
                                                                                                                0,
                                                                                                                $$i
                                                                                                              )
                                                                                                              .concat(
                                                                                                                $$a.slice(
                                                                                                                  $$i +
                                                                                                                    1
                                                                                                                )
                                                                                                              )
                                                                                                          )
                                                                                                      }
                                                                                                    } else {
                                                                                                      _vm.$set(
                                                                                                        accion,
                                                                                                        "check",
                                                                                                        $$c
                                                                                                      )
                                                                                                    }
                                                                                                  }
                                                                                                }
                                                                                              }
                                                                                            ),
                                                                                            _vm._v(
                                                                                              " "
                                                                                            ),
                                                                                            _c(
                                                                                              "label",
                                                                                              {
                                                                                                attrs: {
                                                                                                  for:
                                                                                                    "cssCheckbox" +
                                                                                                    accion.id
                                                                                                }
                                                                                              }
                                                                                            )
                                                                                          ]
                                                                                        )
                                                                                      ]
                                                                                    )
                                                                                  ]
                                                                                )
                                                                              ]
                                                                            )
                                                                          ]
                                                                        )
                                                                      ]
                                                                    }
                                                                  )
                                                                ]
                                                              : [
                                                                  _c(
                                                                    "div",
                                                                    {
                                                                      staticClass:
                                                                        "accordion"
                                                                    },
                                                                    [
                                                                      _c(
                                                                        "div",
                                                                        {
                                                                          staticClass:
                                                                            "card"
                                                                        },
                                                                        [
                                                                          _c(
                                                                            "div",
                                                                            {
                                                                              staticClass:
                                                                                "card-header pointer-cursor d-flex align-items-center collapsed"
                                                                            },
                                                                            [
                                                                              _vm._v(
                                                                                "\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tSin acciones asociadas\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"
                                                                              )
                                                                            ]
                                                                          )
                                                                        ]
                                                                      )
                                                                    ]
                                                                  )
                                                                ]
                                                          ],
                                                          2
                                                        )
                                                      ]
                                                    )
                                                  ]
                                                })
                                              ],
                                              2
                                            )
                                          : _c("div", [
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "card bg-white text-black"
                                                },
                                                [
                                                  _c(
                                                    "div",
                                                    {
                                                      staticClass:
                                                        "card-header pointer-cursor d-flex align-items-center collapsed"
                                                    },
                                                    [
                                                      _vm._v(
                                                        "\n\t\t\t\t\t\t\t\t\t\t\t\t\t\tSin menús asociados\n\t\t\t\t\t\t\t\t\t\t\t\t\t"
                                                      )
                                                    ]
                                                  )
                                                ]
                                              )
                                            ])
                                      ])
                                    ]
                                  )
                                ]
                              )
                            ]
                          })
                        ],
                        2
                      )
                    ]
                  : [
                      _c("div", { staticClass: "accordion" }, [
                        _c("div", { staticClass: "card bg-dark text-white" }, [
                          _c(
                            "div",
                            {
                              staticClass:
                                "card-header bg-dark-darker pointer-cursor d-flex align-items-center collapsed"
                            },
                            [
                              _vm._v(
                                "\n\t\t\t\t\t\t\t\t\tBusqueda finalizada. No se han encontrado datos\n\t\t\t\t\t\t\t\t"
                              )
                            ]
                          )
                        ])
                      ])
                    ]
              ],
              2
            )
          ])
        ],
        2
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js":
/*!********************************************************************!*\
  !*** ./node_modules/vue-loader/lib/runtime/componentNormalizer.js ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return normalizeComponent; });
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode /* vue-cli only */
) {
  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () {
        injectStyles.call(
          this,
          (options.functional ? this.parent : this).$root.$options.shadowRoot
        )
      }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functional component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ "./resources/js/app/Sistema/Administradores/Permisos.vue":
/*!***************************************************************!*\
  !*** ./resources/js/app/Sistema/Administradores/Permisos.vue ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Permisos_vue_vue_type_template_id_5a929dbb___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Permisos.vue?vue&type=template&id=5a929dbb& */ "./resources/js/app/Sistema/Administradores/Permisos.vue?vue&type=template&id=5a929dbb&");
/* harmony import */ var _Permisos_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Permisos.vue?vue&type=script&lang=js& */ "./resources/js/app/Sistema/Administradores/Permisos.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Permisos_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Permisos_vue_vue_type_template_id_5a929dbb___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Permisos_vue_vue_type_template_id_5a929dbb___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/app/Sistema/Administradores/Permisos.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/app/Sistema/Administradores/Permisos.vue?vue&type=script&lang=js&":
/*!****************************************************************************************!*\
  !*** ./resources/js/app/Sistema/Administradores/Permisos.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Permisos_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Permisos.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/app/Sistema/Administradores/Permisos.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Permisos_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/app/Sistema/Administradores/Permisos.vue?vue&type=template&id=5a929dbb&":
/*!**********************************************************************************************!*\
  !*** ./resources/js/app/Sistema/Administradores/Permisos.vue?vue&type=template&id=5a929dbb& ***!
  \**********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Permisos_vue_vue_type_template_id_5a929dbb___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Permisos.vue?vue&type=template&id=5a929dbb& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/app/Sistema/Administradores/Permisos.vue?vue&type=template&id=5a929dbb&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Permisos_vue_vue_type_template_id_5a929dbb___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Permisos_vue_vue_type_template_id_5a929dbb___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);