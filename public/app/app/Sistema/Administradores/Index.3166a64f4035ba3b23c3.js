(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["app/Sistema/Administradores/Index"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/app/Sistema/Administradores/Index.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/app/Sistema/Administradores/Index.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      error: [],
      success: [],
      url: {
        current: this.$root.base_url + this.$route.path,
        permisos: {}
      },
      rows: {
        data: {},
        total: 0,
        last_page: 1
      },
      selects: {
        estados: [{
          label: 'Activo',
          value: 1
        }, {
          label: 'Inactivo',
          value: 0
        }]
      },
      filters: {
        id: null,
        nombre: null,
        email: null,
        estado: null
      },
      valid: {
        password: '',
        confirmPassword: ''
      },
      password: {
        value: null,
        confirmed: null
      },
      table: {
        filtrando: false,
        showActions: true
      },
      usuario: null
    };
  },
  mounted: function () {
    var _mounted = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              this.filtrar(); //this.permisos = await this.$root.permissions('/');

            case 1:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, this);
    }));

    function mounted() {
      return _mounted.apply(this, arguments);
    }

    return mounted;
  }(),
  computed: {
    inhabilitarCambioClave: function inhabilitarCambioClave() {
      return !(this.valid.password == 'is-valid' && this.valid.confirmPassword == 'is-valid');
    }
  },
  methods: {
    limpiarMensajes: function limpiarMensajes() {
      this.success = [];
      this.error = [];
    },
    limpiarValid: function limpiarValid() {
      this.valid = {
        password: '',
        confirmPassword: ''
      };
    },
    limpiarPassword: function limpiarPassword() {
      this.password = {
        value: null,
        confirmed: null
      };
    },
    filtrar: function filtrar() {
      var _this = this;

      var page = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;
      this.limpiarMensajes();
      this.table.filtrando = true;
      var load = loading(this);
      var request = new FormData();
      request.append('page', page);
      this.filters.id && request.append('id', this.filters.id);
      this.filters.nombre && request.append('nombre', this.filters.nombre);
      this.filters.email && request.append('email', this.filters.email);
      this.filters.estado && request.append('estado', this.filters.estado.value);
      axios.post(this.url.current, request).then(function (response) {
        _this.rows = response.data.administradores;
      })["catch"](function (error) {
        _this.error = _this.$root.arrayResponse(error);
      })["finally"](function () {
        load.hide();
        _this.table.filtrando = false;
      });
    },
    limpiar: function limpiar() {
      this.filters = {
        id: null,
        nombre: null,
        email: null,
        estado: null
      };
      this.filtrar(1);
    },
    verificarEstatus: function verificarEstatus(estado) {
      var activo;

      if (estado == 1) {
        activo = '<span class="badge badge-success">Activo</span>';
      } else {
        activo = '<span class="badge badge-danger">Inactivo</span>';
      }

      return activo;
    },
    editar: function editar(id) {
      window.location = this.url.current + '/' + id + '/editar';
    },
    verCambiarClave: function verCambiarClave(key) {
      var _this2 = this;

      this.limpiarMensajes();
      this.usuario = this.rows.data[key];
      $('#modalCambiarClave').modal();
      $('#modalCambiarClave').on('hide.bs.modal', function () {
        _this2.usuario = null;

        _this2.limpiarPassword();

        _this2.limpiarValid();
      });
    },
    cambiarClave: function cambiarClave() {
      var _this3 = this;

      var load = loading(this);
      var request = new FormData();
      request.append('id', this.usuario.id);
      this.password.value && request.append('password', this.password.value);
      this.password.confirmed && request.append('password_confirmation', this.password.confirmed);
      axios.post(this.url.current + '/editar_clave', request).then(function (response) {
        _this3.success = response.data.success;
      })["catch"](function (error) {
        _this3.error = _this3.$root.arrayResponse(error);
      })["finally"](function () {
        load.hide();
        window.scroll(0, 0);
        $('#modalCambiarClave').modal('hide');
      });
    },
    verPermisosPerfiles: function verPermisosPerfiles(id) {
      window.location = this.url.current + '/' + id + '/permisos';
    },
    desactivar: function desactivar(key) {
      var _this4 = this;

      this.limpiarMensajes();
      var usuario = this.rows.data[key];
      this.$swal({
        text: '¿Esta seguro que desea deshabilitar el administrador "' + usuario.nombre_completo + '"?',
        icon: 'warning',
        showConfirmButton: true,
        showCancelButton: false
      }).then(function (response) {
        if (response.value) {
          var load = loading(_this4);
          axios.post(_this4.url.current + '/desactivar', {
            'id': usuario.id
          }).then(function (response) {
            _this4.success = response.data.success;
            _this4.rows.data[key] = response.data.admin;
          })["catch"](function (error) {
            _this4.error = _this4.$root.arrayResponse(error);
          })["finally"](function () {
            load.hide();
          });
        }
      });
    },
    reactivar: function reactivar(key) {
      var _this5 = this;

      this.limpiarMensajes();
      var usuario = this.rows.data[key];
      this.$swal({
        text: '¿Esta seguro que desea reactivar el administrador "' + usuario.nombre_completo + '"?',
        icon: 'warning',
        showConfirmButton: true,
        showCancelButton: false
      }).then(function (response) {
        if (response.value) {
          var load = loading(_this5);
          axios.post(_this5.url.current + '/reactivar', {
            'id': usuario.id
          }).then(function (response) {
            _this5.success = response.data.success;
            _this5.rows.data[key] = response.data.admin;
          })["catch"](function (error) {
            _this5.error = _this5.$root.arrayResponse(error);
          })["finally"](function () {
            load.hide();
          });
        }
      });
    },
    validarPassword: function validarPassword() {
      if (this.password.value) {
        if (this.password.value.toString().length > 7) {
          this.valid.password = 'is-valid';
        } else {
          this.valid.password = 'is-invalid';
        }
      }
    },
    validarConfirmPassword: function validarConfirmPassword() {
      if (this.password.confirmed) {
        if (this.password.confirmed.toString().length > 7 && this.password.value == this.password.confirmed) {
          this.valid.confirmPassword = 'is-valid';
        } else {
          this.valid.confirmPassword = 'is-invalid';
        }
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/app/Sistema/Administradores/Index.vue?vue&type=template&id=0bf97436&":
/*!*************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/app/Sistema/Administradores/Index.vue?vue&type=template&id=0bf97436& ***!
  \*************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("alertas", { attrs: { success: _vm.success, error: _vm.error } }),
      _vm._v(" "),
      _c(
        "panel",
        { attrs: { type: "filtro" } },
        [
          _c("template", { slot: "header" }, [
            _vm._v("\n\t\t\t\tFiltros\n\t\t\t")
          ]),
          _vm._v(" "),
          _c("template", { slot: "buttons" }, [
            _c(
              "button",
              {
                staticClass: "btn btn-warning btn-sm",
                on: { click: _vm.limpiar }
              },
              [_vm._v("Limpiar")]
            ),
            _vm._v(" "),
            _c(
              "button",
              {
                staticClass: "btn btn-success btn-sm",
                on: { click: _vm.filtrar }
              },
              [_vm._v("Filtrar")]
            )
          ]),
          _vm._v(" "),
          _c("template", { slot: "main" }, [
            _c("div", { staticClass: "row" }, [
              _c(
                "div",
                {
                  staticClass: "form-group col-xs-12 col-sm-2 col-md-1 col-lg-1"
                },
                [
                  _c("label", { staticClass: "font-weight-bold" }, [
                    _vm._v("ID")
                  ]),
                  _vm._v(" "),
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.filters.id,
                        expression: "filters.id"
                      }
                    ],
                    staticClass: "form-control",
                    attrs: { type: "text" },
                    domProps: { value: _vm.filters.id },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(_vm.filters, "id", $event.target.value)
                      }
                    }
                  })
                ]
              ),
              _vm._v(" "),
              _c(
                "div",
                {
                  staticClass: "form-group col-xs-12 col-sm-4 col-md-3 col-lg-3"
                },
                [
                  _c("label", { staticClass: "font-weight-bold" }, [
                    _vm._v("Nombre")
                  ]),
                  _vm._v(" "),
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.filters.nombre,
                        expression: "filters.nombre"
                      }
                    ],
                    staticClass: "form-control",
                    attrs: { type: "text" },
                    domProps: { value: _vm.filters.nombre },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(_vm.filters, "nombre", $event.target.value)
                      }
                    }
                  })
                ]
              ),
              _vm._v(" "),
              _c(
                "div",
                {
                  staticClass: "form-group col-xs-12 col-sm-4 col-md-3 col-lg-3"
                },
                [
                  _c("label", { staticClass: "font-weight-bold" }, [
                    _vm._v("Email")
                  ]),
                  _vm._v(" "),
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.filters.email,
                        expression: "filters.email"
                      }
                    ],
                    staticClass: "form-control",
                    attrs: { type: "text" },
                    domProps: { value: _vm.filters.email },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(_vm.filters, "email", $event.target.value)
                      }
                    }
                  })
                ]
              ),
              _vm._v(" "),
              _c(
                "div",
                {
                  staticClass: "form-group col-xs-12 col-sm-4 col-md-3 col-lg-3"
                },
                [
                  _c("label", { staticClass: "font-weight-bold" }, [
                    _vm._v("Estado")
                  ]),
                  _vm._v(" "),
                  _c("v-select", {
                    attrs: { options: _vm.selects.estados },
                    model: {
                      value: _vm.filters.estado,
                      callback: function($$v) {
                        _vm.$set(_vm.filters, "estado", $$v)
                      },
                      expression: "filters.estado"
                    }
                  })
                ],
                1
              )
            ])
          ])
        ],
        2
      ),
      _vm._v(" "),
      _c(
        "panel",
        {
          attrs: {
            type: "crud",
            "footer-class": _vm.rows.last_page == 1 ? "d-none" : ""
          }
        },
        [
          _c("template", { slot: "header" }, [
            _vm._v(
              "\n\t\t\t\tAdministradores (" +
                _vm._s(_vm.rows.total) +
                " registrados)\n\t\t\t"
            )
          ]),
          _vm._v(" "),
          _c("template", { slot: "buttons" }, [
            _c(
              "a",
              {
                staticClass: "btn btn-success btn-sm",
                attrs: { href: _vm.url.current + "/crear" }
              },
              [_vm._v("Crear nuevo")]
            )
          ]),
          _vm._v(" "),
          _c(
            "template",
            { slot: "main" },
            [
              _c("div", { staticClass: "table-responsive" }, [
                _c("table", { staticClass: "table table-bordered" }, [
                  _c("thead", [
                    _c("tr", [
                      _c("th", [_vm._v("ID")]),
                      _vm._v(" "),
                      _c("th", [_vm._v("Nombre")]),
                      _vm._v(" "),
                      _c("th", [_vm._v("Email")]),
                      _vm._v(" "),
                      _c("th", [_vm._v("País")]),
                      _vm._v(" "),
                      _c("th", [_vm._v("Cedula")]),
                      _vm._v(" "),
                      _c("th", [_vm._v("Teléfono")]),
                      _vm._v(" "),
                      _c("th", [_vm._v("Estado")]),
                      _vm._v(" "),
                      _c("th", [_vm._v("Fecha creación")]),
                      _vm._v(" "),
                      _vm.table.showActions
                        ? _c("th", [_vm._v("Acciones")])
                        : _vm._e()
                    ])
                  ]),
                  _vm._v(" "),
                  _c(
                    "tbody",
                    [
                      _vm.table.filtrando
                        ? [
                            _c("tr", [
                              _c(
                                "td",
                                {
                                  attrs: {
                                    colspan: _vm.table.showActions ? 8 : 7
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n\t               \t\t\t\t\t\tBuscando datos\n\t               \t\t\t\t\t"
                                  )
                                ]
                              )
                            ])
                          ]
                        : !_vm.table.filtrando &&
                          Object.keys(_vm.rows.data).length > 0
                        ? _vm._l(_vm.rows.data, function(value, index) {
                            return _c("tr", [
                              _c("td", [_vm._v(_vm._s(value.id))]),
                              _vm._v(" "),
                              _c("td", [_vm._v(_vm._s(value.nombre))]),
                              _vm._v(" "),
                              _c("td", [_vm._v(_vm._s(value.email))]),
                              _vm._v(" "),
                              _c("td", [
                                _vm._v(
                                  _vm._s(value._pais ? value._pais.nombre : "-")
                                )
                              ]),
                              _vm._v(" "),
                              _c("td", [_vm._v(_vm._s(value.cedula))]),
                              _vm._v(" "),
                              _c("td", [
                                _vm._v(
                                  _vm._s(value.telefono ? value.telefono : "-")
                                )
                              ]),
                              _vm._v(" "),
                              _c("td", [
                                _c("span", {
                                  domProps: {
                                    innerHTML: _vm._s(
                                      _vm.verificarEstatus(value.activo)
                                    )
                                  }
                                })
                              ]),
                              _vm._v(" "),
                              _c("td", [
                                _vm._v(
                                  _vm._s(_vm._f("dateTime")(value.created_at))
                                )
                              ]),
                              _vm._v(" "),
                              _c("td", [
                                _c(
                                  "button",
                                  {
                                    staticClass: "btn btn-info btn-sm",
                                    attrs: {
                                      title: "Editar administrador #" + value.id
                                    },
                                    on: {
                                      click: function($event) {
                                        return _vm.editar(value.id)
                                      }
                                    }
                                  },
                                  [_c("i", { staticClass: "fa fa-edit fa-fw" })]
                                ),
                                _vm._v(" "),
                                !value.provider
                                  ? _c(
                                      "button",
                                      {
                                        staticClass: "btn btn-warning btn-sm",
                                        attrs: {
                                          title:
                                            "Cambiar clave de usuario #" +
                                            value.id
                                        },
                                        on: {
                                          click: function($event) {
                                            return _vm.verCambiarClave(index)
                                          }
                                        }
                                      },
                                      [
                                        _c("i", {
                                          staticClass: "fa fa-lock fa-fw"
                                        })
                                      ]
                                    )
                                  : _vm._e(),
                                _vm._v(" "),
                                _c(
                                  "button",
                                  {
                                    staticClass: "btn btn-primary btn-sm",
                                    attrs: {
                                      title:
                                        "Administrar acciones asociadas a perfil #" +
                                        value.id
                                    },
                                    on: {
                                      click: function($event) {
                                        return _vm.verPermisosPerfiles(value.id)
                                      }
                                    }
                                  },
                                  [
                                    _c("i", {
                                      staticClass: "fa fa-list-alt fa-fw"
                                    })
                                  ]
                                ),
                                _vm._v(" "),
                                value.activo
                                  ? _c(
                                      "button",
                                      {
                                        staticClass: "btn btn-danger btn-sm",
                                        attrs: {
                                          title:
                                            "Desactivar administrador #" +
                                            value.id
                                        },
                                        on: {
                                          click: function($event) {
                                            return _vm.desactivar(index)
                                          }
                                        }
                                      },
                                      [
                                        _c("i", {
                                          staticClass: "fa fa-ban fa-fw"
                                        })
                                      ]
                                    )
                                  : _vm._e(),
                                _vm._v(" "),
                                !value.activo
                                  ? _c(
                                      "button",
                                      {
                                        staticClass: "btn btn-success btn-sm",
                                        attrs: {
                                          title:
                                            "Reactivar administrador #" +
                                            value.id
                                        },
                                        on: {
                                          click: function($event) {
                                            return _vm.reactivar(index)
                                          }
                                        }
                                      },
                                      [
                                        _c("i", {
                                          staticClass: "fa fa-check fa-fw"
                                        })
                                      ]
                                    )
                                  : _vm._e()
                              ])
                            ])
                          })
                        : [
                            _c("tr", [
                              _c(
                                "td",
                                {
                                  attrs: {
                                    colspan: _vm.table.showActions ? 8 : 7
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n\t\t               \t\t\t\t\tBusqueda finalizada. No se han encontrado datos\n\t\t               \t\t\t\t"
                                  )
                                ]
                              )
                            ])
                          ]
                    ],
                    2
                  )
                ])
              ]),
              _vm._v(" "),
              _c("pagination", {
                attrs: { data: _vm.rows, align: "right" },
                on: { "pagination-change-page": _vm.filtrar }
              })
            ],
            1
          )
        ],
        2
      ),
      _vm._v(" "),
      _c(
        "modal",
        { attrs: { id: "modalCambiarClave", type: "detalles" } },
        [
          _c("template", { slot: "header" }, [
            _vm._v(
              "\n\t\t\t\tCambiar clave de usuario " +
                _vm._s(
                  _vm.usuario && _vm.usuario.email ? _vm.usuario.email : ""
                ) +
                "\n        \t"
            )
          ]),
          _vm._v(" "),
          _c("template", { slot: "main" }, [
            _vm.usuario
              ? _c("div", { staticClass: "row" }, [
                  _c(
                    "div",
                    {
                      staticClass:
                        "form-group col-xs-12 col-sm-12 col-md-12 col-lg-12"
                    },
                    [
                      _c("label", [
                        _vm._v("Clave "),
                        _c("span", { staticClass: "text-red" }, [_vm._v("*")]),
                        _vm._v(" "),
                        _c("i", {
                          staticClass: "fa fa-info-circle",
                          attrs: {
                            title:
                              "La clave debe contener al menos 8 caracteres"
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.password.value,
                            expression: "password.value"
                          }
                        ],
                        staticClass: "form-control",
                        class: _vm.valid.password,
                        attrs: { type: "password" },
                        domProps: { value: _vm.password.value },
                        on: {
                          blur: function($event) {
                            return _vm.validarPassword()
                          },
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(_vm.password, "value", $event.target.value)
                          }
                        }
                      })
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      staticClass:
                        "form-group col-xs-12 col-sm-12 col-md-12 col-lg-12"
                    },
                    [
                      _c("label", [
                        _vm._v("Confirmar clave "),
                        _c("span", { staticClass: "text-red" }, [_vm._v("*")])
                      ]),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.password.confirmed,
                            expression: "password.confirmed"
                          }
                        ],
                        staticClass: "form-control",
                        class: _vm.valid.confirmPassword,
                        attrs: { type: "password" },
                        domProps: { value: _vm.password.confirmed },
                        on: {
                          keyup: function($event) {
                            return _vm.validarConfirmPassword()
                          },
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              _vm.password,
                              "confirmed",
                              $event.target.value
                            )
                          }
                        }
                      })
                    ]
                  )
                ])
              : _vm._e()
          ]),
          _vm._v(" "),
          _c("template", { slot: "footer" }, [
            _c(
              "button",
              {
                staticClass: "btn btn-success btn-sm",
                attrs: { disabled: _vm.inhabilitarCambioClave },
                on: {
                  click: function($event) {
                    return _vm.cambiarClave()
                  }
                }
              },
              [_vm._v("Cambiar clave")]
            )
          ])
        ],
        2
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js":
/*!********************************************************************!*\
  !*** ./node_modules/vue-loader/lib/runtime/componentNormalizer.js ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return normalizeComponent; });
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode /* vue-cli only */
) {
  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () {
        injectStyles.call(
          this,
          (options.functional ? this.parent : this).$root.$options.shadowRoot
        )
      }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functional component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ "./resources/js/app/Sistema/Administradores/Index.vue":
/*!************************************************************!*\
  !*** ./resources/js/app/Sistema/Administradores/Index.vue ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Index_vue_vue_type_template_id_0bf97436___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Index.vue?vue&type=template&id=0bf97436& */ "./resources/js/app/Sistema/Administradores/Index.vue?vue&type=template&id=0bf97436&");
/* harmony import */ var _Index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Index.vue?vue&type=script&lang=js& */ "./resources/js/app/Sistema/Administradores/Index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Index_vue_vue_type_template_id_0bf97436___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Index_vue_vue_type_template_id_0bf97436___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/app/Sistema/Administradores/Index.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/app/Sistema/Administradores/Index.vue?vue&type=script&lang=js&":
/*!*************************************************************************************!*\
  !*** ./resources/js/app/Sistema/Administradores/Index.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Index.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/app/Sistema/Administradores/Index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/app/Sistema/Administradores/Index.vue?vue&type=template&id=0bf97436&":
/*!*******************************************************************************************!*\
  !*** ./resources/js/app/Sistema/Administradores/Index.vue?vue&type=template&id=0bf97436& ***!
  \*******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_template_id_0bf97436___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Index.vue?vue&type=template&id=0bf97436& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/app/Sistema/Administradores/Index.vue?vue&type=template&id=0bf97436&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_template_id_0bf97436___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_template_id_0bf97436___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);