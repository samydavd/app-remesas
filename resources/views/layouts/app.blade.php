<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Styles -->
        <link href="{{ asset('css/custom-theme.min.css') }}" rel="stylesheet" />
        <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    </head>
    <body>
        <div id="app">
            <div id="page-container" class="fade page-header-fixed has-scroll">

                @include('partials.header')
                @include('partials.sidebar')
                    
                <main id="content" class="content">
                    @yield('content')
                </main>

                @include('partials.footer')
            </div>
        </div>

        @yield('script')
        <!-- ================== BEGIN BASE JS ================== -->
        <script src="{{ asset('js/custom-theme.min.js') }}"></script>
        <!-- ================== END BASE JS ================== -->
    </body>
</html>
