import cliente from '_routes/cliente.js';
import sistema from '_routes/sistema.js';
import mantenedor from '_routes/mantenedor.js';

const baseRoutes =  [
	...cliente,
	...sistema,
	...mantenedor
];

export default baseRoutes;