// CONFIGURACIÓN BÁSICA DE VUE
require('./bootstrap');

window.Vue = require('vue');

//============================================================
//=============CONFIGURACIÓN PERSONALIZADA DE VUE=============
//============================================================

// DETERMINANDO MODO PRODUCCION
let basePath = '/',
    app_name = process.env.MIX_NAME,
    base_url = process.env.MIX_URL;

if (process.env.NODE_ENV == 'production') {
    basePath = '/';
    app_name = 'Remesas';
    base_url = 'http://isnotjs.com/app-remesas';
}

// IMPORTANDO DEPENDENCIAS
import VueRouter from 'vue-router';
import Loading from 'vue-loading-overlay';
import VueSweetalert2 from 'vue-sweetalert2';
import OptionsSweetalert2 from './config/sweetalert2';

// IMPORTANDO ESTILOS
import 'vue-loading-overlay/dist/vue-loading.css';
import 'sweetalert2/dist/sweetalert2.min.css';
import 'vue-select/dist/vue-select.css';

require('./config/components');
require('./config/filters');

// SET GLOBAL DEL LOADER
window.loading = function(self, ...args) {
    return self.$loading.show({
        loader: 'spinner',
        opacity: 0.4,
        backgroundColor: '#4C4C4C',
        zIndex: 9999,
        ...args[0]
    });
};

// SET DEPENDENCIAS
Vue.use(VueRouter);
Vue.use(Loading);
Vue.use(VueSweetalert2, OptionsSweetalert2);

// SET ROUTER
import routes from './config/routes';
import methods from './config/methods';

const router = new VueRouter({
    base: basePath,
    mode: 'history',
    routes, // short for routes: routes
})

// CONFIGURACIÓN VUE
const app = new Vue({
    el: '#custom-app',
    router,
    data() {
        return {
            app_name: app_name,
            base_url: base_url,
            menu: [],
            user: {},
        }
    },
    methods,
});
