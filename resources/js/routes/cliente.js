const routes = [
	// TRANSFERENCIAS PENDIENTES
	{
		path: '/cliente/transferencias',
		component: () => import(/* webpackChunkName: 'app/Cliente/Transferencia/Index' */ '_app/Cliente/Transferencias/Index')
	},
	// TRANSFERENCIAS FINALIZADAS
	{
		path: '/cliente/transferencias/finalizadas',
		component: () => import(/* webpackChunkName: 'app/Cliente/Transferencia/Finalizada' */ '_app/Cliente/Transferencias/Finalizada')
	},
	// TRANSFERENCIAS RECHAZADAS
	{
		path: '/cliente/transferencias/rechazadas',
		component: () => import(/* webpackChunkName: 'app/Cliente/Transferencia/Rechazada' */ '_app/Cliente/Transferencias/Rechazada')
	},
];

export default routes;
