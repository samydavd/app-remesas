const routes = [
	// TIPOS Y SUBTIPOS
	{
		path: '/mantenedor/tipos',
		component: () => import(/* webpackChunkName: 'app/Mantenedor/Tipos/Index' */ '_app/Mantenedor/Tipos/Index')
	},
	{
		path: '/mantenedor/tipos/:idTipo/subtipos',
		component: () => import(/* webpackChunkName: 'app/Mantenedor/Subtipos/Index' */ '_app/Mantenedor/Subtipos/Index')
	},

	// PAISES
	{
		path: '/mantenedor/paises',
		component: () => import(/* webpackChunkName: 'app/Mantenedor/Paises/Index' */ '_app/Mantenedor/Paises/Index')
	},

	// MONEDAS
	{
		path: '/mantenedor/monedas',
		component: () => import(/* webpackChunkName: 'app/Mantenedor/Monedas/Index' */ '_app/Mantenedor/Monedas/Index')
	},
	// TASAS
	{
		path: '/mantenedor/tasas',
		component: () => import(/* webpackChunkName: 'app/Mantenedor/Tasas/Index' */ '_app/Mantenedor/Tasas/Index')
	},
	// Bancos
	{
		path: '/mantenedor/bancos',
		component: () => import(/* webpackChunkName: 'app/Mantenedor/Bancos/Index' */ '_app/Mantenedor/Bancos/Index')
	},
];

export default routes;
