<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCltBancosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('clt_bancos');
        Schema::create('clt_bancos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('razon_social', 255)->comment('Nombre del banco');
            $table->unsignedInteger('pais_id')->nullable()->comment('País de residencia. Referenciado a clt_paises');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('pais_id')->references('id')->on('clt_paises');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clt_bancos');
    }
}
