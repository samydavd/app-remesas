<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComSubtiposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('com_subtipos');
        Schema::create('com_subtipos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre', 80)->comment('Nombre del subtipo');
            $table->string('apodo', 80)->comment('Apodo del subtipo en sistema');
            $table->unsignedInteger('tipo_id')->comment('Referenciado a com_tipos');

            $table->string('descripcion')
            ->nullable(true)
            ->comment('Descripción breve del significado del subtipo');

            $table->timestamps();
            $table->softDeletes();

            $table->index('apodo');
            $table->foreign('tipo_id')->references('id')->on('com_tipos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('com_subtipos');
    }
}
