<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComImagenesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('com_imagenes');
        Schema::create('com_imagenes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre', 250)->comment('Nombre del archivo en el sistema');
            $table->string('nombre_original', 250)->comment('Nombre a mostrar del usuario');
            $table->string('ruta', 250)->comment('Ruta en el storage del archivo');
            
            $table->integer('imageable_id')->comment('ID relacional al model para el morph');
            $table->string('imageable_type')->comment('Ruta del modelo al que apunta');

            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('com_imagenes');
    }
}
