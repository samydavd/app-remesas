<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCltCuentasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('clt_cuentas');
        Schema::create('clt_cuentas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('razon_social', 255)->comment('Nombre de la persona o empresa');
            $table->string('cedula', 15)->comment('Número de documento de identidad');
            $table->unsignedInteger('banco_id')->comment('Banco asociado a la cuenta. Referenciado a clt_bancos');
            $table->unsignedInteger('id_tipo_cuenta')->comment('Tipo de cuenta del banco. Referenciado a com_subtipos');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('banco_id')->references('id')->on('clt_bancos');
            $table->foreign('id_tipo_cuenta')->references('id')->on('com_subtipos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clt_cuentas');
    }
}
