<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCltTransferenciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('clt_transferencias');
        Schema::create('clt_transferencias', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('razon_social', 255)->comment('Nombre del destinatario');
            $table->string('cedula', 15)->comment('Número de documento de identidad');
            $table->unsignedInteger('banco_id')->comment('Banco asociado a la cuenta. Referenciado a clt_bancos');
            $table->unsignedInteger('id_tipo_cuenta')->comment('Tipo de cuenta del banco. Referenciado a com_subtipos');
            $table->unsignedBigInteger('usuario_id')->comment('Usuario que efectua la transferencia. Referenciado a clt_usuarios');
            $table->decimal('monto_envio', 20, 2)->comment('Monto mínimo permitido para cambiar');
            $table->unsignedInteger('id_moneda_envio')->comment('Referenciado a com_subtipos');
            $table->decimal('monto_recibido', 12, 2)->comment('Monto mínimo permitido para cambiar');
            $table->unsignedInteger('id_moneda_recibo')->comment('Referenciado a com_subtipos');
            $table->unsignedInteger('id_tipo_operacion')->comment('Referenciado a com_subtipos');
            $table->unsignedInteger('id_estado')->comment('Referenciado a com_subtipos');
            $table->string('tasa', 25)->comment('Factor de cambio');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('banco_id')->references('id')->on('clt_bancos');
            $table->foreign('id_tipo_cuenta')->references('id')->on('com_subtipos');
            $table->foreign('usuario_id')->references('id')->on('clt_usuarios');
            $table->foreign('id_moneda_envio')->references('id')->on('com_subtipos');
            $table->foreign('id_moneda_recibo')->references('id')->on('com_subtipos');
            $table->foreign('id_tipo_operacion')->references('id')->on('com_subtipos');
            $table->foreign('id_estado')->references('id')->on('com_subtipos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clt_transferencias');
    }
}
