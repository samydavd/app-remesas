<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateComPaisesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('com_paises', function (Blueprint $table) {
            if(!Schema::hasColumn('com_paises', 'bandera')) {
                $table->string('bandera', 100)->after('moneda_id')->comment('URL de bandera del pais');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('com_paises', function (Blueprint $table) {
            if(Schema::hasColumn('com_paises', 'bandera')) {
                $table->dropColumn('bandera');
            }
        });
    }
}
