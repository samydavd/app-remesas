<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCltRechazosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('clt_rechazos');
        Schema::create('clt_rechazos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('transferencia_id')->comment('Referenciado a clt_transferencias');
            $table->text('motivo')->comment('Motivo del rechazo de la transferencia');
            $table->dateTime('fecha_resolucion')->nullable()->comment('Fecha de resolucion del problema');
            $table->timestamp('created_at');

            $table->foreign('transferencia_id')->references('id')->on('clt_transferencias');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clt_rechazos');
    }
}
