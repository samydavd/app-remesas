<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\SoftDeletes;

class CreateAdmMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('adm_menus');
        Schema::create('adm_menus', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('ruta');
            $table->integer('seccion_id')->unsigned();
            $table->smallInteger('orden');
            $table->string('icono');
            $table->text('descripcion')->nullable(true);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('seccion_id')->references('id')->on('adm_secciones');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adm_menus');
    }
}
