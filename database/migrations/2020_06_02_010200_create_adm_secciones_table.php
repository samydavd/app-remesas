<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\SoftDeletes;

class CreateAdmSeccionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('adm_secciones');
        Schema::create('adm_secciones', function (Blueprint $table) {            
            $table->increments('id');
            $table->string('nombre');
            $table->string('ruta');
            $table->smallInteger('orden');
            $table->string('icono');
            $table->text('descripcion')->nullable(true);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adm_secciones');
    }
}
