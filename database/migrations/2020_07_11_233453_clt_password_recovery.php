<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Migrations\softDeletes;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CltPasswordRecovery extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   
        Schema::dropIfExists('clt_password_recovery');
        Schema::create('clt_password_recovery', function( Blueprint $table){
            $table->id();
            $table->unsignedInteger('usuario_id');
            $table->string('email',100);
            $table->string('token',250);
            $table->timestamp('created_at');
            $table->timestamp('expires_at')->nullable();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clt_password_recovery');
    }
}
