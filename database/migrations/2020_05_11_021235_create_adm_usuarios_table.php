<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdmUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('adm_usuarios');
        Schema::create('adm_usuarios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre');
            $table->string('apellidos')->nullable();
            $table->string('cedula', 15)->nullable()->comment('Número de documento de identidad');
            $table->string('email')->unique();
            $table->unsignedInteger('pais_id')->nullable()->comment('País de residencia. Referenciado a clt_paises');
            $table->string('password', 80);
            $table->string('telefono', 20)->nullable()->comment('Telefono de contacto');
            $table->boolean('activo')->default(1);
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('pais_id')->references('id')->on('clt_paises');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adm_usuarios');
    }
}
