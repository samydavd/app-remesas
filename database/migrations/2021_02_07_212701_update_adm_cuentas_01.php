<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAdmCuentas01 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('adm_cuentas', function(Blueprint $table){
            if(!Schema::hasColumn('adm_cuentas', 'numero_cuenta')){
                $table->string('numero_cuenta',250);
            }  

            if(!Schema::hasColumn('adm_cuentas', 'estado')){
                $table->boolean('estado')->default(1);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('adm_cuentas', function(Blueprint $table){
            if(Schema::hasColumn('adm_cuentas', 'numero_cuenta')){
                $table->dropColumn('numero_cuenta');
            }  

            if(Schema::hasColumn('adm_cuentas', 'estado')){
                $table->dropColumn('estado');
            }
        });
    }
}
