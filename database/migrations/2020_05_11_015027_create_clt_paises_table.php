<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCltPaisesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('clt_paises');
        Schema::create('clt_paises', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre', 80)->comment('Nombre del país');
            $table->unsignedInteger('id_moneda')->comment('Referenciado a com_subtipos');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_moneda')->references('id')->on('com_subtipos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clt_paises');
    }
}
