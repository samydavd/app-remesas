<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCltTransferenciasDetallesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('clt_transferencias_detalles');
        Schema::create('clt_transferencias_detalles', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('transferencia_id');

            $table->string('razon_social', 255)
            ->comment('Nombre del destinatario');

            $table->string('cedula', 15)
            ->comment('Número de documento de identidad del destinatario');

            $table->unsignedInteger('id_tipo_cuenta')
            ->comment('Tipo de cuenta del banco. Referenciado a com_subtipos');

            $table->unsignedInteger('banco_id')
            ->comment('Banco a transferir. Referenciado a clt_bancos');

            $table->decimal('monto_envio', 20, 2)
            ->comment('Monto enviado al destino de la transferencia');

            $table->unsignedInteger('moneda_envio_id')
            ->comment('Moneda del monto enviado al destino de la transferencia. Referenciado a com_subtipos');

            $table->decimal('monto_recibido', 12, 2)
            ->comment('Monto que recibirá el destinatario de la transferencia');

            $table->unsignedInteger('moneda_recibo_id')
            ->comment('Moneda del monto que recibirá el destinatario. Referenciado a com_subtipos');

            $table->unsignedInteger('id_estado')
            ->comment('Referenciado a com_subtipos');

            $table->timestamps();

            $table->foreign('transferencia_id')->references('id')->on('clt_transferencias');
            $table->foreign('banco_id')->references('id')->on('clt_bancos');
            $table->foreign('moneda_envio_id')->references('id')->on('com_subtipos');
            $table->foreign('moneda_recibo_id')->references('id')->on('com_subtipos');
            $table->foreign('id_estado')->references('id')->on('com_subtipos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clt_transferencias_detalles');
    }
}
