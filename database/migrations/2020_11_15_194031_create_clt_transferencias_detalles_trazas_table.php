<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCltTransferenciasDetallesTrazasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('clt_transferencias_detalles_trazas');
        Schema::create('clt_transferencias_detalles_trazas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('transferencia_detalle_id')
            ->comment('Referenciado a clt_transferencias_detalles');

            $table->unsignedInteger('id_estado')->comment('Referenciado a com_subtipos');
            $table->timestamp('created_at');

            $table->foreign('transferencia_detalle_id', 'trazas_transferencia_detalle_id_foreign')
            ->references('id')
            ->on('clt_transferencias_detalles');

            $table->foreign('id_estado')->references('id')->on('com_subtipos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clt_transferencias_detalles_trazas');
    }
}
