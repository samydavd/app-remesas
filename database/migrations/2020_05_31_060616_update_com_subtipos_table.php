<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateComSubtiposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('com_subtipos', function(Blueprint $table) {
            if(Schema::hasColumn('com_subtipos', 'apodo')) {
                $table->dropColumn('apodo');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('com_subtipos', function(Blueprint $table) {
            if(!Schema::hasColumn('com_subtipos', 'apodo')) {
                $table->string('apodo', 80)->comment('Apodo del subtipo en sistema');
            }
        });
    }
}
