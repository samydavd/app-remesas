<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAdmUsuariosTable02 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('adm_usuarios', function (Blueprint $table) {
            if(!Schema::hasColumn('adm_usuarios', 'removable')) {
                $table->boolean('removable')->after('activo')->default(true);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('adm_usuarios', function (Blueprint $table) {
            if(Schema::hasColumn('adm_usuarios', 'removable')) {
                $table->dropColumn('removable');
            }
        });
    }
}
