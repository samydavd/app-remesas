<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCltUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('clt_usuarios');
        Schema::create('clt_usuarios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre');
            $table->string('apellidos')->nullable();
            $table->string('cedula', 15)->nullable()->comment('Número de documento de identidad');
            $table->string('email')->unique();
            $table->string('avatar')->nullable()->comment('Enlace de avatar por RRSS');
            $table->unsignedInteger('pais_id')->nullable()->comment('País de residencia. Referenciado a clt_paises');
            $table->string('password', 80)->nullable();
            $table->timestamp('email_verified_at')->nullable()->comment('Fecha hora de verificación de correo');
            $table->string('telefono', 20)->nullable()->comment('Telefono de contacto');
            $table->string('provider')->nullable()->comment('Nombre RRSS del origen de afiliación');
            $table->string('provider_id')->nullable()->comment('ID RRSS del origen de afiliación');
            $table->boolean('notificacion')->default(0)->comment('Marca para recibir notificaciones');
            $table->boolean('activo')->default(1);
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('pais_id')->references('id')->on('clt_paises');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clt_usuarios');
    }
}
