<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateCltCuentasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clt_cuentas', function (Blueprint $table) {
            $table->string('numero_cuenta', 30)->after('cedula')
            ->comment('Número de cuenta asociado');

            $table->unsignedBigInteger('usuario_id')->after('id_tipo_cuenta')
            ->comment('Usuario que registra número de cuenta');

            $table->foreign('usuario_id')->references('id')->on('clt_usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clt_cuentas', function (Blueprint $table) {
            if(Schema::hasColumn('clt_cuentas', 'numero_cuenta')) {
                $table->dropColumn('numero_cuenta');
            }

            if(Schema::hasColumn('clt_cuentas', 'usuario_id')) {
                $table->dropForeign(['usuario_id']);
                $table->dropColumn('usuario_id');
            }
        });
    }
}
