<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateCltTransferenciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clt_transferencias', function (Blueprint $table) {
            $table->string('numero_cuenta', 30)->after('cedula')
            ->comment('Número de cuenta asociado')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clt_transferencias', function (Blueprint $table) {
            if(Schema::hasColumn('clt_transferencias', 'numero_cuenta')) {
                $table->dropColumn('numero_cuenta');
            }
        });
    }
}
