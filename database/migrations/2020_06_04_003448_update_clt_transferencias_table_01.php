<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateCltTransferenciasTable01 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clt_transferencias', function (Blueprint $table) {
            if(Schema::hasColumn('clt_transferencias', 'razon_social')) {
                $table->dropColumn('razon_social');
            }
            if(Schema::hasColumn('clt_transferencias', 'cedula')) {
                $table->dropColumn('cedula');
            }
            if(Schema::hasColumn('clt_transferencias', 'numero_cuenta')) {
                $table->dropColumn('numero_cuenta');
            }
            if(Schema::hasColumn('clt_transferencias', 'banco_id')) {
                $table->dropForeign(['banco_id']);
                $table->dropColumn('banco_id');
            }
            if(Schema::hasColumn('clt_transferencias', 'id_tipo_cuenta')) {
                $table->dropForeign(['id_tipo_cuenta']);
                $table->dropColumn('id_tipo_cuenta');
            }
            if(Schema::hasColumn('clt_transferencias', 'monto_envio')) {
                $table->decimal('monto_envio', 20, 2)
                ->comment('Monto total de la tranferencia')
                ->change();

                $table->renameColumn('monto_envio', 'monto');
            }
            if(Schema::hasColumn('clt_transferencias', 'id_moneda_envio')) {
                $table->dropForeign(['id_moneda_envio']);
                $table->renameColumn('id_moneda_envio', 'moneda_id');
            }
            if(Schema::hasColumn('clt_transferencias', 'monto_recibido')) {
                $table->dropColumn('monto_recibido');
            }
            if(Schema::hasColumn('clt_transferencias', 'id_moneda_recibo')) {
                $table->dropForeign(['id_moneda_recibo']);
                $table->dropColumn('id_moneda_recibo');
            }
            if(Schema::hasColumn('clt_transferencias', 'id_estado')) {
                $table->dropForeign(['id_estado']);
                $table->dropColumn('id_estado');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clt_transferencias', function (Blueprint $table) {
            if(!Schema::hasColumn('clt_transferencias', 'razon_social')) {
                $table->string('razon_social', 255)->comment('Nombre del destinatario');
            }

            if(!Schema::hasColumn('clt_transferencias', 'cedula')) {
                $table->string('cedula', 15)->comment('Número de documento de identidad');
            }

            if(!Schema::hasColumn('clt_transferencias', 'numero_cuenta')) {
                $table->string('numero_cuenta', 30)
                ->after('cedula')
                ->comment('Número de cuenta asociado');
            }

            if(!Schema::hasColumn('clt_transferencias', 'banco_id')) {
                $table->unsignedInteger('banco_id')
                ->comment('Banco asociado a la cuenta. Referenciado a clt_bancos');

                $table->foreign('banco_id')->references('id')->on('clt_bancos');
            }

            if(!Schema::hasColumn('clt_transferencias', 'id_tipo_cuenta')) {
                $table->unsignedInteger('id_tipo_cuenta')
                ->comment('Tipo de cuenta del banco. Referenciado a com_subtipos');

                $table->foreign('id_tipo_cuenta')->references('id')->on('com_subtipos');
            }

            if(!Schema::hasColumn('clt_transferencias', 'monto_envio')) {
                $table->decimal('monto', 20, 2)
                ->comment('Monto total de la tranferencia')
                ->change();

                $table->renameColumn('monto', 'monto_envio');
            }

            if(!Schema::hasColumn('clt_transferencias', 'id_moneda_envio')) {
                $table->dropForeign(['moneda_id']);
                $table->renameColumn('moneda_id', 'id_moneda_envio');
                $table->foreign('id_moneda_envio')->references('id')->on('com_subtipos');
            }

            if(!Schema::hasColumn('clt_transferencias', 'monto_recibido')) {
                $table->decimal('monto_recibido', 12, 2)
                ->comment('Monto mínimo permitido para cambiar');
            }

            if(!Schema::hasColumn('clt_transferencias', 'id_moneda_recibo')) {
                $table->unsignedInteger('id_moneda_recibo')
                ->comment('Referenciado a com_subtipos');

                $table->foreign('id_moneda_recibo')->references('id')->on('com_subtipos');
            }

            if(!Schema::hasColumn('clt_transferencias', 'id_estado')) {
                $table->unsignedInteger('id_estado')
                ->comment('Referenciado a com_subtipos');

                $table->foreign('id_estado')->references('id')->on('com_subtipos');
            }
        });
    }
}
