<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateCltCuentasUpdate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clt_cuentas', function (Blueprint $table) {
            if (!Schema::hasColumn('clt_cuentas', 'pais_id')) {
                $table->unsignedInteger('pais_id')
                ->after('id_tipo_cuenta')
                ->comment('País asociado a la cuenta. Referenciado a com_paises');
            }

            if(!$table->hasForeign(['pais_id'])) {
                $table->foreign('pais_id')->references('id')->on('com_paises');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clt_cuentas', function (Blueprint $table) {
            if($table->hasForeign(['pais_id'])) {
                $table->dropForeign(['pais_id']);
            }

            if (Schema::hasColumn('clt_cuentas', 'pais_id')) {
                $table->dropColumn('pais_id');
            }
        });
    }
}
