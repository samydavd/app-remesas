<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateCltTiposCuentasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clt_tipos_cuentas', function(Blueprint $table){
            $table->unsignedInteger('id_estado')
            ->nullable()
            ->after('id_tipo_cuenta')
            ->comment('Estado del tipo de cuenta');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clt_tipos_cuentas', function(Blueprint $table){
            $table->dropColumn('id_estado');
        });
    }
}
