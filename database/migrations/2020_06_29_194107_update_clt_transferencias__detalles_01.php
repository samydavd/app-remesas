<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateCltTransferenciasDetalles01 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clt_transferencias_detalles', function(Blueprint $table){
            if(!Schema::hasColumn('clt_transferencias_detalles', 'cuenta_id')) {
                $table->unsignedInteger('cuenta_id')
                ->nullable()
                ->after('id_estado')
                ->comment('Numero de cuenta asociado');
            }
            if(!Schema::hasColumn('clt_transferencias_detalles', 'numero_cuenta')) {
                $table->integer('numero_cuenta')->after('cuenta_id')->comment('numero de cuenta transferido');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clt_transferencias_detalles', function(Blueprint $table){
            if(Schema::hasColumn('clt_transferencias_detalles', 'cuenta_id')) {
                $table->dropColumn('cuenta_id');
            }
            if(Schema::hasColumn('clt_transferencias_detalles','numero_cuenta')){
                $table->dropColumn('numero_cuenta');
            }
        });
    }
}
