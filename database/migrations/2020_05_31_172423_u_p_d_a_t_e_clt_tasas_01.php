<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UPDATECltTasas01 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clt_tasas', function(Blueprint $table){
            $table->boolean('estado')->default(1)->comment('estado de la tasa');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clt_tasas', function(Blueprint $table){
            $table->dropColumn('estado');
        });
    }
}
