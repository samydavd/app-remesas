<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateComPaisesTable02 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('com_paises', function (Blueprint $table) {
            if(Schema::hasColumn('com_paises', 'id_moneda')) {

                if($table->hasForeign('clt_paises_id_moneda_foreign')) {
                    $table->dropForeign('clt_paises_id_moneda_foreign');
                }
                else {
                    $table->dropForeign(['id_moneda']);
                }

                $table->renameColumn('id_moneda', 'moneda_id');
                $table->foreign('moneda_id')->references('id')->on('clt_monedas');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('com_paises', function (Blueprint $table) {
            if(Schema::hasColumn('com_paises', 'id_moneda')) {
                $table->renameColumn('moneda_id', 'id_moneda');
                $table->foreign('id_moneda')->references('id')->on('clt_monedas');
            }
        });
    }
}
