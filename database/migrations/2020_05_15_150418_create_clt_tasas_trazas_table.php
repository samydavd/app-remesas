<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCltTasasTrazasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('clt_tasas_trazas');
        Schema::create('clt_tasas_trazas', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('pais_origen_id')->comment('Referenciado a clt_paises');
            $table->unsignedInteger('pais_destino_id')->comment('Referenciado a clt_paises');
            $table->string('tasa', 25)->comment('Factor de cambio');
            $table->decimal('monto_min', 12, 2)->nullable()->comment('Monto mínimo permitido para cambiar');
            $table->timestamp('created_at');

            $table->foreign('pais_origen_id')->references('id')->on('clt_paises');
            $table->foreign('pais_destino_id')->references('id')->on('clt_paises');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clt_tasas_trazas');
    }
}
