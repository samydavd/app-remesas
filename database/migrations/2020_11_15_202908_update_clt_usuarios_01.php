<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateCltUsuarios01 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clt_usuarios', function(Blueprint  $table){
            if(!Schema::hasColumn('clt_usuarios', 'password_temporal')){
                $table->string('password_temporal')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clt_usuarios', function(Blueprint  $table){
            if(Schema::hasColumn('clt_usuarios', 'password_temporal')){
                $table->dropColumn('password_temporal');
            }
        });
    }
}
