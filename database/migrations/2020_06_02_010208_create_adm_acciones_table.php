<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\SoftDeletes;

class CreateAdmAccionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('adm_acciones');
        Schema::create('adm_acciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('accion');
            $table->integer('menu_id')->unsigned();
            $table->smallInteger('orden');
            $table->text('descripcion')->nullable(true);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('menu_id')->references('id')->on('adm_menus');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adm_acciones');
    }
}
