<?php
namespace App\Http\Requests\Api\Transferencia;

use Illuminate\Foundation\Http\FormRequest;

class Transferencia extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    function rules()
    {
        return [
            'monto' => 'required|numeric',
            'id_tipo_operacion' => 'required|integer|exists:com_subtipos,id',
            'file' => 'required',
            'origen' => 'required|integer|exists:com_paises,id',
            'destino' => 'required|integer|exists:com_paises,id',
            'detalle' => 'required',
            'detalle.*.cuenta_id' => 'required|integer',
            'detalle.*.razon_social' => 'required|string',
            'detalle.*.cedula' => 'required|numeric',
            'detalle.*.numero_cuenta' => 'required|integer',
            'detalle.*.banco_id' => 'required|integer',
            'detalle.*.id_tipo_cuenta' => 'required|integer',
            'detalle.*.monto_envio' => 'required|numeric',
        ];
    }

    function attributes()
    {
        return [
            'detalle' => 'Datos de Transferencia',
            'detalle.*.cuenta_id' => 'Cuenta',
            'detalle.*.razon_social' => 'Razon social',
            'detalle.*.cedula' => 'Cedula',
            'detalle.*.numero_cuenta' => 'Numero de Cuenta',
            'detalle.*.banco_id' => 'Banco',
            'detalle.*.id_tipo_cuenta' => 'Tipo de cuenta',
            'detalle.*.monto_envio' => 'Monto envio',
        ];
    } 
}
