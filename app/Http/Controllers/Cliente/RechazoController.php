<?php

namespace App\Http\Controllers\Cliente;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Cliente\Rechazo;
use App\Models\Cliente\Usuario;
use App\Models\Cliente\Transferencia;
use App\Models\Cliente\TransferenciaDetalle;

class RechazoController extends Controller {

	public function vue () 
	{
		return view('vue');
	}

	public function index (Request $request)
    {
        $detalle = TransferenciaDetalle::buscar($request)
        ->with('_banco', '_moneda_envio', '_estado')
        ->with(['_transferencia' => function($q) {
            $q->with('_pais_origen', '_pais_destino');
        }])
        ->where('id_estado', 7)
        ->paginate();

        return response([
            'detalle' => $detalle
        ], 201);
    }

    public function rechazos (Request $request)
    {
    	$this->validate($request, [
    		'id' => 'required|numeric'
    	]);

    	try {
	    	$rechazos = Rechazo::where('transferencia_detalle_id', $request->id)->get();

	    	return response([
	    		'rechazos' => $rechazos
	    	], 201);
    	}
    	catch(\Exception $e) {
            $mensaje = $e->getLine().' '.$e->getMessage();
            return response(['error' => $mensaje], 500);
        }
    }
}