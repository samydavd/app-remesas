<?php

namespace App\Http\Controllers\Cliente;

use DB;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Interfaces\CrudSimple;

use App\Models\Cliente\Banco;
use App\Models\Cliente\BancoTipoCuenta;
use App\Models\Comun\Pais;
use App\Models\Comun\Subtipo;

class BancoController extends Controller implements CrudSimple
{
    public function vue()
    {
    	return view('vue');
    }

    public function index(Request $request)
    {
        try {
            //DB::enablequeryLog();
            $bancos = Banco::buscar($request)
            ->with('_pais','_tipos')
            ->paginate();

            $tipos_cuentas = Subtipo::selectOptions('tipos_cuentas')
            ->get();

            //dd(DB::getquerylog());
            $paises = Pais::get();

            return response([
                'bancos' => $bancos,
                'paises' => $paises,
                'tipos_cuentas' => $tipos_cuentas
            ], 200);

        } catch (\Exception $e) {

            $mensaje = $e->getLine().' '.$e->getMessage();
            return response(['error'=>$mensaje], 500);
        }
    }

    public function guardar(Request $request)
    {
        $this->validate($request, [
            'pais_id' => 'required|integer',
            'razon_social' => 'required|string',
        ]);

        try {

            DB::beginTransaction();
            if($this->exists($request)) throw new \Exception("Este banco ya existe para este pais");

            $banco = new Banco;
            $banco->fill($request->all());
            $banco->save();

            DB::commit();

            $banco = $banco->load('_pais','_tipos');

            return response([
                'success' => 'Banco almacenado con exito',
                'banco' => $banco,
            ], 200);
        }
        catch(\Exception $e) {

            DB::rollBack();
            $mensaje = $e->getLine().' '.$e->getMessage();
            return response(['error' => $mensaje], 500);
        }
    }

    public function actualizar( Request $request)
    {
        $this->validate($request, [
            'id' => 'required|integer|exists:clt_bancos,id',
            'pais_id' => 'required|integer',
            'razon_social' => 'required|string',
        ]);

        try {

            DB::beginTransaction();

            if($this->exists($request)) throw new \Exception("Este banco ya existe para este pais");

            $banco = Banco::findOrFail($request->id);
            $banco->fill($request->all());
            $banco->save();

            DB::commit();

            $banco->load('_pais','_tipos');

            return response([
                'success' => 'Banco almacenado con exito',
                'banco' => $banco,
            ], 200);

        } catch (\Exception $e) {

            DB::rollBack();
            $mensaje = $e->getLine().' '.$e->getMessage();
            return response(['error' => $mensaje], 500);
        }
    }

    public function eliminar(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|integer|exists:clt_bancos,id'
        ]);

        try {
            DB::beginTransaction();

            $banco = Banco::findOrFail($request->id);
            $banco->delete();

            DB::commit();

            return response([
                'success' => 'Operacion realizada con exito',
            ], 200);
        }
        catch(\Exception $e) {
            DB::rollBack();
            $mensaje = $e->getLine().' '.$e->getMessage();
            return response(['error' => $mensaje], 500);
        }
    }

    public function asignar(Request $request)
    {
        try {
            $exists = DB::table('clt_tipos_cuentas')->where('banco_id', $request->id)
            ->where('id_tipo_cuenta', $request->id_tipo_cuenta)
            ->first();

            if ($exists) throw new \Exception("Operacion Invalida");

            $tipocuentas = new BancoTipoCuenta;
            $tipocuentas->id_tipo_cuenta = $request->id_tipo_cuenta;
            $tipocuentas->id_estado = 1; //activo
            $tipocuentas->banco_id = $request->id;
            $tipocuentas->save();

            $banco = Banco::with('_tipos','_pais')->find($request->id);

            DB::commit();
            return response([
                'success' => 'Tipo de cuenta asignado con exito.',
                'banco' => $banco,
            ]);

        } catch (\Exception $e) {

            DB::rollback();
            return response([
                'error' => $e->getMessage(),
            ],500);
        }
    }

    public function desactivarTipo( Request $request)
    {
        try {
            $tipo = BancoTipoCuenta::find($request->id_tipo);
            $tipo->id_estado = 2; //desactivado
            $tipo->save();

            DB::commit();
            return response([
                'success' => 'Tipo de cuenta desactivada.',
            ]);

        } catch (\Exception $e) {

            DB::rollback();
            return response([
                'error' => $e->getMessage(),
            ],500);
        }
    }

    public function activarTipo( Request $request)
    {
        try {
            $tipo = BancoTipoCuenta::find($request->id_tipo);
            $tipo->id_estado = 1; //activo
            $tipo->save();

            DB::commit();
            return response([
                'success' => 'Tipo de cuenta activada.',
            ]);

        } catch (\Exception $e) {

            DB::rollback();
            return response([
                'error' => $e->getMessage(),
            ],500);
        }
    }


    private function exists(Request $request)
    {
        $exists = Banco::where('pais_id', $request->pais_id)
        ->where(DB::raw('UPPER(razon_social)'), strtoupper($request->razon_social))
        ->when($request->id, function($q) use ($request){
            $q->where('id', '!=' , $request->id);
        })
        ->first();

        return $exists;
    }
}
