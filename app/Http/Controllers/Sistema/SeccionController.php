<?php

namespace App\Http\Controllers\Sistema;

use DB;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Interfaces\CrudComplete;

use App\Models\Sistema\Seccion;
use App\Helpers\FontAwesome;

class SeccionController extends Controller implements CrudComplete
{
    public function vue()
    {
    	return view('vue');
    }

    public function index(Request $request)
    {
		try {
			$secciones = Seccion::buscar($request)
			->orderBy('orden')
			->paginate();

			return response(['secciones' => $secciones], 200);

		} 
		catch (\Exception $e) {
			$mensaje = $e->getLine().' '.$e->getMessage();
			return response(['error' => $mensaje], 500);
		}
		
    }

    public function crear(Request $request) 
    {
		try {
			$iconos = FontAwesome::listIcons();
			
			return response(['iconos' => $iconos], 200);
			
		} 
		catch (\Exception $e) {
			$mensaje = $e->getLine().' '.$e->getMessage();
			return response(['error' => $mensaje], 500);
		}
	}

	public function editar(Request $request) 
    {
		try {
			$seccion = Seccion::find($request->id);
	    	$iconos = FontAwesome::listIcons();

			return response(compact('seccion', 'iconos'), 200);
			
		} 
		catch (\Exception $e) {
			$mensaje = $e->getLine().' '.$e->getMessage();
			return response(['error' => $mensaje], 500);
		}
	    
	}

	public function guardar(Request $request)
	{
		$this->validate($request, [
			'nombre' => 'required|string',
			'ruta' => 'required|string',
			'icono' => 'required|string',
			'orden' => 'required|numeric'
		]);

		try {
			DB::beginTransaction();

			$seccion = new Seccion;
			$seccion->fill($request->all());
			$seccion->save();

			DB::commit();

			$mensaje = ['Sección guardada exitosamente'];
			return response(['success' => $mensaje], 200);
		}
		catch(\Exception $e) {
			DB::rollBack();
			$mensaje = $e->getLine().' '.$e->getMessage();
			return response(['error' => $mensaje], 500);
		}
	}

	public function actualizar(Request $request)
	{
		$this->validate($request, [
			'id' => 'required|exists:adm_secciones',
			'nombre' => 'required|string',
			'ruta' => 'required|string',
			'icono' => 'required|string',
			'orden' => 'required|numeric'
		]);

		try {
			DB::beginTransaction();

			$seccion = Seccion::findOrFail($request->id);
			$seccion->fill($request->all());
			$seccion->save();

			DB::commit();

			$mensaje = ['Sección actualizada exitosamente'];
			return response(['success' => $mensaje], 200);
		}
		catch(\Exception $e) {
			DB::rollBack();
			$mensaje = $e->getLine().' '.$e->getMessage();
			return response(['error' => $mensaje], 500);
		}
	}

	public function eliminar(Request $request)
	{
		$this->validate($request, [
			'id' => 'required|exists:adm_secciones'
		]);

		try {
			DB::beginTransaction();
			$seccion = Seccion::findOrFail($request->id);
			$seccion->delete();
			DB::commit();

			$mensaje = ['Sección eliminada exitosamente'];
			return response(['success' => $mensaje], 200);
		}
		catch(\Exception $e) {
			DB::rollBack();
			$mensaje = $e->getLine().' '.$e->getMessage();
			return response(['error' => $mensaje], 500);
		}
	}
}
