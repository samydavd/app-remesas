<?php

namespace App\Http\Controllers\Sistema;

use DB;
use Hash;
use Validator;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Interfaces\Maintainer;
use App\Models\Cliente\Usuario;

use App\Models\Comun\Subtipo;
use App\Models\Cliente\TransferenciaDetalle;

class ClientController extends Controller implements Maintainer
{
    public function vue()
    {
    	return view('vue');
    }

    public function index(Request $request)
    {
        try {

            $rows = Usuario::with('_pais')
            ->withCount('_transferencias_detalles as transferencias')
            ->withCount(['_transferencias_detalles as procesando' => function($q){
                $q->where('id_estado', 6);
            }])
            ->withCount(['_transferencias_detalles as rechazadas' => function($q){
                $q->where('id_estado', 7);
            }])
            ->withCount(['_transferencias_detalles as finalizadas' => function($q){
                $q->where('id_estado', 9 );
            }])
            ->orderBy('nombre')
            ->paginate();


            $estados = [
                [ 'label' => 'Activo', 'value' => 1],
                [ 'label' => 'Desactivado', 'value' => 0],
            ];

            $selects = [
                'estados' => $estados
            ]; 

            return response(compact('rows', 'selects'), 200);

        } catch (\Exception $e) {

            return response([
                'error' => $e->getLine().' '.$e->getMessage()
            ], 500);
        }
    }

    public function update(Request $request)
    {
        Validator::make($request->all(),[
            'id' => 'required|exists:clt_usuarios,id',
            'password' => 'required|string|min:8',
        ])
        ->setAttributeNames([
            'id' => 'Identificador',
            'password' => 'Contrasena temporal',
        ])
        ->validate();
        
        try {

            Usuario::where('id', $request->id )
            ->update([
                'password_temporal' => hash::make($request->password)
            ]);

            return response([
                'success' => 'Operacion realizada con exito.'
            ],200);

        } catch (\Exception $e) {

            return response([
                'error' => $e->getLine().' '.$e->getMessage()
            ], 500);
        }
    }

    public function changeState(Request $request)
    {
        Validator::make($request->all(),[
            'id' => 'required|exists:clt_usuarios,id',
        ])
        ->setAttributeNames([
            'id' => 'Identificador',
        ])
        ->validate();
        
        try {

            $usuario = Usuario::with('_pais')
            ->withCount('_transferencias_detalles as transferencias')
            ->withCount(['_transferencias_detalles as procesando' => function($q){
                $q->where('id_estado', 6);
            }])
            ->withCount(['_transferencias_detalles as rechazadas' => function($q){
                $q->where('id_estado', 7);
            }])
            ->withCount(['_transferencias_detalles as finalizadas' => function($q){
                $q->where('id_estado', 9 );
            }])  
            ->find($request->id);

            $usuario->activo  = $usuario->activo ? 0 : 1;
            $usuario->update();
            
            return response([
                'success' => 'Operacion realizada con exito.',
                'usuario' => $usuario,
            ]);

        } catch (\Exception $e) {

            return response([
                'error' => $e->getLine().' '.$e->getMessage()
            ], 500);
        }
    }

    public function detalles( Request $request )
    {
        try {
            
            $rows = TransferenciaDetalle::buscar($request)
            ->whereHas('_usuarios', function($q) use ($request){
                $q->where('clt_usuarios.id', $request->user_id);
            })
            ->with('_banco', '_moneda_envio', '_estado')
            ->with(['_transferencia' => function($q) {
                $q->with('_pais_origen', '_pais_destino');
            }])
            ->paginate();

            
            $selects = [
                'estados' => Subtipo::selectOptions('estados')->get(),
            ];
    
            return response(compact('rows','selects'));

        } catch (\Exception $e) {

            return response([
                'error' => $e->getLine().' '.$e->getMessage()
            ], 500);
        }
    }

    public function getDetalles( Request $request)
    {
        $this->validate($request, [
    		'id' => 'required|integer|exists:clt_transferencias_detalles'
    	]);

    	$detalle = TransferenciaDetalle::with('_banco', '_estado')
    	->with('_tipo_cuenta', '_moneda_envio', '_moneda_recibo', '_imagen', '_rechazo_actual')
    	->with(['_transferencia' => function($q) {
    		$q->with('_pais_origen', '_pais_destino', '_usuario', '_tipo_operacion');
    	}])
    	->find($request->id);

    	return response ([
    		'detalle' => $detalle
    	], 200);
    }
}
