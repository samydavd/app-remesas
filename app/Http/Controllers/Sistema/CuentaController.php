<?php

namespace App\Http\Controllers\Sistema;

use DB;
use Illuminate\Http\Request;
use App\Models\Sistema\Menu;
use App\Models\Sistema\Seccion;
use App\Http\Controllers\Controller;
use App\Models\Sistema\Cuenta;
use App\Models\Comun\Subtipo;
use App\Models\Comun\Pais;
use App\Models\Cliente\Banco;

class CuentaController extends Controller
{
    public function vue()
    {
    	return view('vue');
    }

    public function index(Request $request)
    {
        try {
            $rows = $this->getData($request);
            //$paises = Pais::get();
            //$tipos_cuenta = Subtipo::where('id_tipo', 3)->get(); 

            $bancos = Banco::with('_pais', '_tipos')->get();
            $selects = compact('bancos');

            return response(compact('selects', 'rows'), 200);

        } catch (\Exception $e) {
            throw $e;
        }
    	
    }

    public function crear(Request $request) 
    {
        $this->validate($request,[
            'banco_id' => 'required|exists:clt_bancos,id',
            'id_tipo_cuenta' => 'required|exists:com_subtipos,id',
            'pais_id' => 'required|exists:com_paises,id',
            'razon_social' => 'required|string',
            'cedula' => 'required|string|max:12',
            'numero_cuenta' => 'required|string|max:30'
        ],[],[
            'banco_id' => 'Banco',
            'id_tipo_cuenta' => 'Tipo de Cuenta',
            'pais_id' => 'Pais',
            'razon_social' => 'Razon social',
            'cedula' => 'Cedula',
            'numero_cuenta' => 'Numero de cuenta'
        ]);

        DB::beginTransaction();

        try {   
            $exists = Cuenta::where('numero_cuenta', $request->numero_cuenta)
            ->where('banco_id', $request->banco_id)
            ->first();

            if($exists) {
                return response(['error' => "El numero de cuenta $request->numero_cuenta ya existe para el Banco"],422);
            }

            $cuenta = new Cuenta;
            $cuenta->fill($request->all());
            $cuenta->save();

            
            $rows =  $this->getData();
            $success = 'Operacion realizada con exito';

            DB::commit();
            return response(compact('rows', 'success'), 200);

        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }   
    }

    public function editar(Request $request) 
    {   
        $this->validate($request,[
            'id' => 'required|exists:adm_cuentas,id',
            'banco_id' => 'required|exists:clt_bancos,id',
            'id_tipo_cuenta' => 'required|exists:com_subtipos,id',
            'pais_id' => 'required|exists:com_paises,id',
            'razon_social' => 'required|string',
            'cedula' => 'required|string|max:12',
            'numero_cuenta' => 'required|string|max:30'
        ],[],[
            'id' => 'Identificador',
            'banco_id' => 'Banco',
            'id_tipo_cuenta' => 'Tipo de Cuenta',
            'pais_id' => 'Pais',
            'razon_social' => 'Razon social',
            'cedula' => 'Cedula',
            'numero_cuenta' => 'Numero de cuenta'
        ]);

        DB::beginTransaction();

        try {   
            $exists = Cuenta::where('numero_cuenta', $request->numero_cuenta)
            ->where('banco_id', $request->banco_id)
            ->where('id', '!=', $request->id)
            ->first();
            if($exists) {
                return response(['error' => "El numero de cuenta $request->numero_cuenta ya existe para el Banco"],422);
            }

            $cuenta = Cuenta::find($request->id);
            $cuenta->fill($request->all());
            $cuenta->save();

            $rows =  $this->getData();
            $success = 'Operacion realizada con exito';

            DB::commit();
            return response(compact('rows', 'success'), 200);
            
            
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }   
    }

    public function desactivar(Request $request)
    {
        $this->validate($request,[
            'id' => 'required|exists:adm_cuentas,id',
        ],[],[
            'id' => 'Identificador',
        ]);

        DB::beginTransaction();

        try {
            $cuenta = Cuenta::find($request->id);
            $cuenta->estado = !$cuenta->estado;
            $cuenta->update();
            
            $rows =  $this->getData(null, 'paginate');
            $success = 'Operacion realizada con exito';

            DB::commit();
            return response(compact('rows', 'success'), 200);
            
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    public function getData($request = null, $method = 'paginate')
    {
        $rows = $menus = Cuenta::when($request, function($q) use ($request){
            $q->buscar($request);
        })
        ->with('_tipo_cuenta', '_pais')
        ->with(['_banco' =>function($q){
            $q->with('_pais', '_tipos');

        }])
        ->orderBy('id')
        ->$method();

        return $rows;
    }
}
