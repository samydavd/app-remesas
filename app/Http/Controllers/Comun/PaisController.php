<?php

namespace App\Http\Controllers\Comun;

use DB;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Interfaces\CrudSimple;

use App\Models\Cliente\Moneda;
use App\Models\Comun\Pais;

class PaisController extends Controller implements CrudSimple
{
    public function vue()
    {
    	return view('vue');
    }

    public function index(Request $request)
    {
        try {
            $paises = Pais::buscar($request)
            ->with('_moneda')
            ->paginate();

            $monedas = Moneda::get();

            return response([
            	'paises' => $paises,
            	'monedas' => $monedas
            ], 200);
        } 
        catch (\Exception $e) {
            $mensaje = $e->getLine().' '.$e->getMessage();
            return response(['error' => $mensaje], 500);
        }
    }

    public function guardar(Request $request)
    {
        $this->validate($request, [
            'nombre' => 'required|string',
            'moneda_id' => 'required|integer|exists:clt_monedas,id'
        ]);

        try {
            DB::beginTransaction();

            $pais = new Pais;
            $pais->fill($request->all());
            $pais->save();

            DB::commit();

            $pais = $pais->load('_moneda');

            $mensaje = ['País guardado exitosamente'];
            return response(compact('pais', 'mensaje'), 200);
        }
        catch(\Exception $e) {
            DB::rollBack();
            $mensaje = $e->getLine().' '.$e->getMessage();
            return response(['error' => $mensaje], 500);
        }
    }

    public function actualizar(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|integer|exists:com_paises,id',
            'nombre' => 'required|string',
            'moneda_id' => 'required|integer|exists:clt_monedas,id'
        ]);

        try {
            DB::beginTransaction();

            $pais = Pais::find($request->id);
            $pais->fill($request->all());
            $pais->save();

            DB::commit();

            $pais = $pais->load('_moneda');

            $mensaje = ['País actualizado exitosamente'];
            return response(compact('pais', 'mensaje'), 200);
        }
        catch(\Exception $e) {
            DB::rollBack();
            $mensaje = $e->getLine().' '.$e->getMessage();
            return response(['error' => $mensaje], 500);
        }
    }

    public function eliminar(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|integer|exists:com_paises,id'
        ]);

        try {
            DB::beginTransaction();

            $pais = Pais::findOrFail($request->id);
            $pais->delete();

            DB::commit();

            $mensaje = ['Pais eliminado exitosamente'];
            return response(['mensaje' => $mensaje], 200);
        }
        catch(\Exception $e) {
            DB::rollBack();
            $mensaje = $e->getLine().' '.$e->getMessage();
            return response(['error' => $mensaje], 500);
        }
    }
}
