<?php

namespace App\Interfaces;	

use Illuminate\Http\Request;

interface Maintainer
{
	public function update(Request $request);
	public function changeState(Request $request);
}