<?php

namespace App\Interfaces;	

use Illuminate\Http\Request;

interface CrudComplete
{
	public function index(Request $request);
	public function crear(Request $request);
	public function guardar(Request $request);
	public function editar(Request $request);
	public function actualizar(Request $request);
	public function eliminar(Request $request);
}