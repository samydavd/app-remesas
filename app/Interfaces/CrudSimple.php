<?php

namespace App\Interfaces;	

use Illuminate\Http\Request;

interface CrudSimple
{
	public function index(Request $request);
	public function guardar(Request $request);
	public function actualizar(Request $request);
	public function eliminar(Request $request);
}