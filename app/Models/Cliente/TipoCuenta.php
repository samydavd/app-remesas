<?php

namespace App\Models\Cliente;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoCuenta extends Model
{
    use SoftDeletes;

    protected $table = 'clt_tipos_cuentas';

    protected $fillable = [
    	'id', 'banco_id', 'id_tipo_cuenta','id_estado'
    ];

    public function _banco ()
    {
    	return $this->belongsTo('App\Models\Cliente\Banco', 'banco_id');
    }

    public function _tipo_cuenta ()
    {
    	return $this->belongsTo('App\Models\Comun\Subtipo', 'id_tipo_cuenta');
    }
}
