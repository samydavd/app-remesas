<?php

namespace App\Models\Cliente;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Comun\Subtipo;

class Banco extends Model
{
    use SoftDeletes;

    protected $table = 'clt_bancos';

    protected $fillable = [
		'razon_social', 'pais_id'
    ];

    protected $dates = ['created_at','updated_at','deleted_at'];

    public function _pais()
    {
        return $this->belongsTo('App\Models\Comun\Pais', 'pais_id');
    }

    public function _tipos_cuentas()
    {
        return $this->hasMany('App\Models\Cliente\BancoTipoCuenta','banco_id');
    }

    public function _tipos(){

        return $this->belongsToMany(
            Subtipo::class,
            BancoTipoCuenta::class,
            'banco_id',
            'id_tipo_cuenta',
            'id',
            'id'
        )->withPivot('id','id_tipo_cuenta', 'id_estado');
    }

    public function scopeBuscar($query, $request)
    {
        if($request->id){
            $query->where('id', $request->id);
        };

        if($request->pais_id){
            $query->where('pais_id', $request->pais_id);
        };

        return $query;
    }

}
