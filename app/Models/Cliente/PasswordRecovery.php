<?php

namespace App\Models\Cliente;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PasswordRecovery extends Model
{
	use SoftDeletes;

    protected $table = 'clt_password_recovery';
    
    protected $fillable = [
        'token' , 'email', 'usuario_id'
    ];
    
    public $timestamps = false; 
    protected $dates = ['created_at' , 'deleted_at', 'expires_at'];

    public function scopeBuscar($query, $request)
    {
    
    	return $query;
    }
}
