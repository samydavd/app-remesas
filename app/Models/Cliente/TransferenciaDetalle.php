<?php

namespace App\Models\Cliente;

use Illuminate\Database\Eloquent\Model;

class TransferenciaDetalle extends Model
{
    protected $table = 'clt_transferencias_detalles';

    protected $fillable = [
    	'transferencia_id', 'razon_social', 'cedula', 'id_tipo_cuenta', 'banco_id',
        'monto_envio', 'moneda_envio_id', 'monto_recibido', 'moneda_recibo_id',
        'id_estado', 'numero_cuenta'
    ];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    protected static function boot()
    {
        parent::boot();

        self::saved(function ($value) {
            TransferenciaDetalleTraza::create([
                'transferencia_detalle_id' => $value->id,
                'id_estado' => $value->id_estado
            ]);
        });
    }

    public function _transferencia()
    {
        return $this->belongsTo('App\Models\Cliente\Transferencia', 'transferencia_id');
    }

    public function _estado ()
    {
    	return $this->belongsTo('App\Models\Comun\Subtipo', 'id_estado');
    }

    public function _banco ()
    {
    	return $this->belongsTo('App\Models\Cliente\Banco', 'banco_id');
    }

    public function _tipo_cuenta ()
    {
    	return $this->belongsTo('App\Models\Comun\Subtipo', 'id_tipo_cuenta');
    }

    public function _moneda_envio ()
    {
    	return $this->belongsTo('App\Models\Cliente\Moneda', 'moneda_envio_id');
    }

    public function _moneda_recibo ()
    {
    	return $this->belongsTo('App\Models\Cliente\Moneda', 'moneda_recibo_id');
    }

    public function _rechazos ()
    {
        return $this->hasMany('App\Models\Cliente\Rechazo', 'transferencia_detalle_id');
    }

    public function _rechazo_actual ()
    {
        return $this->hasOne('App\Models\Cliente\Rechazo', 'transferencia_detalle_id')
        ->whereNull('fecha_resolucion');
    }

    public function _imagen ()
    {
    	return $this->morphOne('App\Models\Comun\Imagen', 'imageable')->latest();
    }

    public function _usuarios()
    {
        return $this->hasManyThrough(
            Usuario::class,
            Transferencia::class,
            'id',
            'id',
            'transferencia_id',
            'usuario_id'
        );
    }

    public function scopeBuscar( $query,  $request )
    {
    	if($request->id) {
    		$query->where('id', $request->id);
    	}

    	if($request->id_estado) {
    		$query->where('id_estado', $request->id_estado);
    	}

        return $query;
    }
}
