<?php

namespace App\Models\Cliente;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cuenta extends Model
{
    use SoftDeletes;

    protected $table = 'clt_cuentas';

    protected $fillable = [
    	'razon_social', 'cedula', 'numero_cuenta', 'banco_id', 'id_tipo_cuenta', 'usuario_id'
    ];

    public function _banco ()
    {
    	return $this->belongsTo('App\Models\Cliente\Banco', 'banco_id');
    }

    public function _tipo_cuenta ()
    {
        return $this->belongsTo('App\Models\Comun\Subtipo', 'id_tipo_cuenta');
    }

    public function _usuario ()
    {
    	return $this->belongsTo('App\Models\Cliente\Usuario', 'usuario_id');
    }
}
