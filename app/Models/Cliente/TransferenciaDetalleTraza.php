<?php

namespace App\Models\Cliente;

use Illuminate\Database\Eloquent\Model;

class TransferenciaDetalleTraza extends Model
{
    const UPDATED_AT = null;
    protected $table = 'clt_transferencias_detalles_trazas';

    protected $fillable = [
    	'id_estado', 'transferencia_detalle_id'
    ];

    public function _estado ()
    {
    	return $this->belongsTo('App\Models\Comun\Subtipo', 'id_estado');
    }

    public function _transferencia ()
    {
    	return $this->belongsTo('App\Models\Cliente\TransferenciaDetalle', 'transferencia_detalle_id');
    }
}
