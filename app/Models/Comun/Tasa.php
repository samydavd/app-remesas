<?php

namespace App\Models\Comun;

use Illuminate\Database\Eloquent\Model;

class Tasa extends Model
{
    protected $table = 'clt_tasas';

    protected $fillable = [
		'pais_origen_id', 'pais_destino_id', 'tasa', 'monto_min','estado'
    ];

    public $timestamps = false;

    public function _pais_origen()
    {
        return $this->belongsTo('App\Models\Comun\Pais', 'pais_origen_id');
    }

    public function _pais_destino()
    {
        return $this->belongsTo('App\Models\Comun\Pais', 'pais_destino_id');
    }

    public function scopeBuscar($query, $request)
    {
        if($request->id){
            $query->where('id', $request->id);
        };

        if($request->pais_destino_id){
            $query->where('pais_destino_id', $request->pais_destino_id);
        };

        if($request->pais_origen_id){
            $query->where('pais_origen_id', $request->pais_origen_id);
        };

        return $query;
    }

}
