<?php

namespace App\Models\Comun;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subtipo extends Model
{
    use SoftDeletes;

    protected $table = 'com_subtipos';

    protected $fillable = [
    	'nombre', 'apodo', 'tipo_id', 'descripcion'
    ];

    public function _tipo ()
    {
    	return $this->belongsTo('App\Models\Comun\Tipo', 'tipo_id');
    }

    public function scopeSelectOptions($query, $apodo)
    {
        return $query->whereHas('_tipo', function ($q) use ($apodo) {
            $q->where('apodo', $apodo);
        });
    }

    public function scopeSelectEspecificsOptions($query, $apodo, $nombre)
    {
        if(is_array($nombre)) {
            $query = $query->whereIn('nombre', $nombre);
        }
        else {
            $query = $query->where('nombre', $nombre);
        }

        return $query->whereHas('_tipo', function ($q) use ($apodo) {
            $q->where('apodo', $apodo);
        });
    }

    public function scopeBuscar($query, $request)
    {
        if($request->idTipo) {
            $query->where('tipo_id', $request->idTipo);
        }

        if($request->nombre) {
            $query->where('nombre', 'LIKE', '%'.$request->nombre.'%');
        }

        if($request->tipo_id) {
            $query->where('tipo_id', $request->tipo_id);
        }

        return $query;
    }
}
