<?php

namespace App\Models\Comun;

use Illuminate\Database\Eloquent\Model;

class TasaTraza extends Model
{
    protected $table = 'clt_tasas_trazas';

    protected $fillable = [
		'pais_origen_id', 'pais_destino_id', 'tasa', 'monto_min'
    ];

    public $timestamps = false;

    public function _pais_origen()
    {
        return $this->belongsTo('App\Models\Comun\Pais', 'pais_origen_id');
    }

    public function _pais_destino()
    {
        return $this->belongsTo('App\Models\Comun\Pais', 'pais_destino_id');
    }


}
