<?php

namespace App\Models\Comun;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pais extends Model
{
	use SoftDeletes;

    protected $table = 'com_paises';

    protected $fillable = [
    	'nombre', 'moneda_id'
    ];

    public function _moneda ()
    {
    	return $this->belongsTo('App\Models\Cliente\Moneda', 'moneda_id');
    }

    public function scopeBuscar ($query, $request)
    {
        if($request->id) {
            $query->where('id', $request->id);
        }

        if($request->nombre) {
            $query->where('nombre', 'LIKE', '%'.$request->nombre.'%');
        }

        return $query;
    }

	public function _tasa_origen()
	{
		return $this->hasMany(Tasa::class, "pais_origen_id");
	}

	public function _tasa_destino()
	{
		return $this->hasMany(Tasa::class, "pais_destino_id");
	}
}
