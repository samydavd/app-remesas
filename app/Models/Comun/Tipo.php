<?php

namespace App\Models\Comun;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tipo extends Model
{
	use SoftDeletes;
	
    protected $table = 'com_tipos';

    protected $fillable = [
		'nombre', 'apodo', 'descripcion'
    ];

    protected static function boot()
    {
        parent::boot();

        self::deleting(function ($value) {
            Subtipo::where('tipo_id', $value->id)->delete();
        });
    }

    public function _subtipos()
    {
        return $this->hasMany('App\Models\Comun\Subtipo', 'tipo_id');
    }

    public function scopeBuscar($query, $request)
    {
        if($request->id) {
            $query->where('id', $request->id); 
        }

        if($request->nombre) {
            $query->where('nombre', 'LIKE', '%'.$request->nombre.'%');  
        }

        if($request->apodo) {
            $query->where('apodo', 'LIKE', '%'.$request->apodo.'%');  
        }

        return $query;
    }
}
