<?php

namespace App\Models\Sistema;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Seccion extends Model
{
	use softDeletes;

    protected $table = 'adm_secciones';

    protected $fillable = [
    	'nombre', 'ruta', 'orden', 'icono', 'descripcion'
    ];

    public function _menus ()
    {
    	return $this->hasMany(Menu::class, 'seccion_id');
    }

    public function scopeBuscar($query, $request)
    {

        $query->when($request->id, function($q) {
        	$q->where('id', $request->id); 
        });

        $query->when($request->nombre, function($q) {
        	$query->where('nombre', 'LIKE', "%$request->nombre%");  
        });

        $query->when($request->ruta, function($q) {
        	$q->where('ruta', 'LIKE', "%$request->ruta%");
        });

        return $query;
    }
}
