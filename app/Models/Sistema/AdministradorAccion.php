<?php

namespace App\Models\Sistema;

use Illuminate\Database\Eloquent\Model;

class AdministradorAccion extends Model
{
    protected $table = 'adm_usuarios_acciones';

    protected $fillable = [
        'usuario_id', 'accion_id'
    ];
}
