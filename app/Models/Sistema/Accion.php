<?php

namespace App\Models\Sistema;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Accion extends Model
{
	use softDeletes;

    protected $table = 'adm_acciones';

    protected $fillable = [
    	'nombre', 'accion', 'menu_id', 'orden', 'descripcion'
    ];

    public function _menu ()
    {
    	return $this->belongsTo(Menu::class, 'menu_id');
    }

    public function scopeBuscar($query, $request)
    {
        if($request->id) {
            $query->where('id', $request->id); 
        }

        if($request->nombre) {
            $query->where('nombre', 'LIKE', "%$request->nombre%");  
        }

        if($request->menu_id) {
            $query->where('menu_id', $request->menu_id);
        }

        return $query;
    }
}
