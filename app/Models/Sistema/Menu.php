<?php

namespace App\Models\Sistema;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Menu extends Model
{
	use softDeletes;

    protected $table = 'adm_menus';

    protected $fillable = [
    	'nombre', 'ruta', 'seccion_id', 'orden', 'icono', 'descripcion'
    ];

    public function _seccion ()
    {
    	return $this->belongsTo(Seccion::class, 'seccion_id');
    }

    public function _acciones ()
    {
    	return $this->hasMany(Accion::class, 'menu_id');
    }

    public function scopeBuscar($query, $request)
    {
        if($request->id) {
            $query->where('id', $request->id); 
        }

        if($request->nombre) {
            $query->where('nombre', 'LIKE', "%$request->nombre%");  
        }

        if($request->ruta) {
            $query->where('ruta', 'LIKE', "%$request->ruta%");
        }

        if($request->seccion_id) {
            $query->where('seccion_id', $request->seccion_id);
        }

        return $query;
    }
}
