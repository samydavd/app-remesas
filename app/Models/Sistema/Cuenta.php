<?php

namespace App\Models\Sistema;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cuenta extends Model
{
	use softDeletes;

    protected $table = 'adm_cuentas';

    protected $fillable = [	
        'razon_social', 'cedula', 'pais_id', 'banco_id', 'id_tipo_cuenta', 'created_at', 'updated_at', 'deleted_at'
        ,'numero_cuenta', 'estado'
    ];

    public function _tipo_cuenta()
    {
        return $this->belongsTo('App\Models\Comun\Subtipo', 'id_tipo_cuenta');
    }

    public function _pais()
    {
        return $this->belongsTo('App\Models\Comun\Pais', 'pais_id');
    }

    public function _banco()
    {
        return $this->belongsTo('App\Models\Cliente\Banco', 'banco_id');
    }
        
    public function scopeBuscar($query, $request)
    {
        if($request->id) {
            $query->where('id', $request->id); 
        }

        if($request->banco_id) {
            $query->where('banco_id', $request->banco_id);   
        }

        if($request->id_tipo_cuenta) {
            $query->where('id_tipo_cuenta', $request->id_tipo_cuenta);
        }

        return $query;
    }
}


