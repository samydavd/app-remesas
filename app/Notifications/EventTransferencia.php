<?php
namespace App\Notifications;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;


class EventTransferencia implements ShouldBroadcast
{
  use Dispatchable, InteractsWithSockets, SerializesModels;

  public $message;
  public $user;

  public function __construct($message , $user)
  {
      $this->message = $message;
      $this->user = base64_encode($user);

  }

  public function broadcastOn()
  {
      return ['canal-transferencias'];
  }

  public function broadcastAs()
  {
      return 'transferencia';
  }

  
}