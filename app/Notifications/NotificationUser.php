<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;

use App\Notifications\CustomMailMessage;

class NotificationUser extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($parametros = [])
    {
        $this->parametros = $parametros;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $mail = new CustomMailMessage;

        foreach($this->parametros as $key => $parametro) {
            switch ($key) {
                case 'from':
                    $mail = $mail->from($parametro);
                    break;

                case 'to':
                    $mail = $mail->to($parametro);
                    break;

                case 'subject':
                    $mail = $mail->subject($parametro);
                    break;

                case 'view':
                    $mail = $mail->view($parametro);
                    break;

                case 'title':
                    $mail = $mail->title($parametro);
                    break;

                case 'message':
                    $mail = $mail->line($parametro);
                    break;

                case 'image':
                    $mail = $mail->imageUrl($parametro);
                    break;

                case 'action':
                    $mail = $mail->action($parametro['texto'], url($parametro['url']));
                    break;

                case 'attach':
                    $mail = $mail->attach($parametro);
                    break;
            }
        }

        return $mail;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
