<?php

namespace App\Notifications;

use Illuminate\Notifications\Messages\MailMessage;

class CustomMailMessage extends MailMessage
{
	/**
     * URL image for the message.
     *
     * @var array|string
     */

	public $imageUrl;

    /**
     * Title for the message.
     *
     * @var array|string
     */

    public $title;

	/**
     * Set URL image.
     *
     * @var string
     * @return $this
     */

	public function imageUrl($imageUrl)
    {
        $this->imageUrl = $imageUrl;

        return $this;
    }

    /**
     * Title for the message.
     *
     * @var string
     * @return $this
     */

    public function title($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get an array representation of the message.
     *
     * @return array
     */

    public function toArray()
    {
        $toArray = parent::toArray();

        $customArray = [
            'title' => $this->title,
            'imageUrl' => $this->imageUrl
        ];

        return array_merge($toArray, $customArray);
    }

}