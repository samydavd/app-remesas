<?php

namespace App\Providers;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //   
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        // Metodos blueprint personalizados

        Blueprint::macro('hasForeign', function ($index) {
            $indexString = is_array($index) ? $this->createIndexName('foreign', $index) : $index;

            $doctrineTable = Schema::getConnection()
                ->getDoctrineSchemaManager()
                ->listTableDetails($this->table);

            return $doctrineTable->hasIndex($indexString);
        });

        // Validaciones personalizadas

        Validator::extend('modulo11', function ($attribute, $value, $parameters) {
            // Se quitan los puntos del valor entregado en caso de que vengan en rut
            $val = strtoupper(preg_replace('/\./', '', $value));

            // Se separa en rut y digito validador
            $tmp = strrpos($value, '-') ? explode('-', $value) : [];

            if (count($tmp) != 2) {
                $tmp[0] = $parameters && $parameters[0] ? substr($val, 0, strlen($val) - 1) : $val;
                $tmp[1] = $parameters && $parameters[0] ? substr($val, strlen($val) - 1, strlen($val)) : null;
            }

            $numero = (int)$tmp[0];
            $dv = $tmp[1];

            // Validando con algoritmo del Modulo 11
            $total = 0; 
            $por = 2; 
            $x = 0;

            for($i = strlen($numero) - 1; $i >= 0; $i--) {
                if($por > 7) { $por = 2; }
                $total += (int)substr($numero, $i, 1) * $por;
                $por++;
            }

            $resto = $total % 11;
            $ref = 11 - $resto;
            $dv_r = '';

            if($resto === 1) { $dv_r = 'K'; }
            else if($resto === 0) { $dv_r = 0; }
            else { $dv_r = $ref; }

            return $dv_r == $dv;
        });

        \Carbon\Carbon::setLocale(config('app.locale'));
    }
}
