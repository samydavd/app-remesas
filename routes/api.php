<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('Api')->group(function(){
    Route::post('register', 'UserController@register');
    Route::post('login', 'UserController@login');
    Route::post('recovery', 'UserController@recovery');
    Route::post('recovery/email', 'UserController@recoveryEmail');
    
    Route::middleware('auth:api')->group(function(){
        Route::post('user', 'UserController@user');
        Route::post('logout', 'UserController@logout');

        // cuenta bancaria
        Route::post('cuentas', 'CuentaController@index');
        Route::post('cuentas/registrar', 'CuentaController@registrar');
        Route::post('cuentas/delete', 'CuentaController@delete');

        // transferencia
        Route::post('transferir', 'TransferenciaController@index');
        Route::post('transferir/guardar', 'TransferenciaController@guardar');
        Route::post('transferir/eliminar', 'TransferenciaController@eliminar');
        Route::post('transferir/actualizar', 'TransferenciaController@actualizar');

        //tasas
        Route::post('tasas', 'TasasController@index');

        //Historial
        Route::post('historial', 'HistorialController@index');

    });

});
