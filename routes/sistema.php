<?php

// RUTAS SIN PERMISOS
Route::namespace('Sistema')->prefix('sistema')->name('sistema')->group(function() {
	
	// USUARIOS
	Route::prefix('administradores')->name('.administradores')->group(function() {
		//
	});
});

// RUTAS CON PERMISOS
Route::middleware('auth')->group(function() {
	Route::namespace('Sistema')->prefix('sistema')->name('sistema')->group(function() {

		// USUARIOS
		Route::prefix('administradores')->name('.administradores')->group(function() {
			Route::get('/', 'AdministradorController@vue')->name('');
			Route::post('/', 'AdministradorController@index')->name('');
			Route::get('crear', 'AdministradorController@vue')->name('.crear');
			Route::post('crear', 'AdministradorController@crear')->name('.crear');
			Route::get('{id}/editar', 'AdministradorController@vue')->name('.editar');
			Route::post('editar', 'AdministradorController@editar')->name('.editar');
			Route::post('guardar', 'AdministradorController@guardar')->name('.guardar');
			Route::post('actualizar', 'AdministradorController@actualizar')->name('.actualizar');
			Route::post('desactivar', 'AdministradorController@desactivar')->name('.desactivar');
			Route::post('reactivar', 'AdministradorController@reactivar')->name('.reactivar');
			Route::post('editar_clave', 'AdministradorController@editar_clave')->name('.editar_clave');

			// PERMISOS DE USUARIOS
			Route::prefix('{id}/permisos')->name('.permisos')->group(function() {
				Route::get('/', 'AdministradorController@vue')->name('.index');
				Route::post('/', 'AdministradorController@permisos_listado')->name('.listado');
				Route::post('guardar', 'AdministradorController@permisos_guardar')->name('.guardar');
			});
		});

		// Clientes 
		Route::prefix('clientes')->name('.permisos')->group(function(){
			Route::get('/', 'ClientController@vue')->name('.index');
			Route::post('/', 'ClientController@index')->name('.index');
			Route::post('change_state', 'ClientController@changeState')->name('.disabled');
			Route::post('update', 'ClientController@update')->name('.password');
			Route::get('detalles/{user_id}', 'ClientController@vue')->name('.detalles');
			Route::post('detalles/{user_id}', 'ClientController@detalles')->name('.detalles');
			Route::post('detalles/{user_id}/detalles', 'ClientController@getDetalles')->name('.detalles');
		}); 

		// SECCIONES
		Route::prefix('secciones')->name('.secciones')->group(function() {
			Route::get('/', 'SeccionController@vue')->name('');
			Route::post('/', 'SeccionController@index')->name('');
			Route::get('crear', 'SeccionController@vue')->name('.crear');
			Route::post('crear', 'SeccionController@crear')->name('.crear');
			Route::get('{id}/editar', 'SeccionController@vue')->name('.editar');
			Route::post('editar', 'SeccionController@editar')->name('.editar');
			Route::post('guardar', 'SeccionController@guardar')->name('.guardar');
			Route::post('actualizar', 'SeccionController@actualizar')->name('.actualizar');
			Route::post('eliminar', 'SeccionController@eliminar')->name('.eliminar');
			
			// MENUS
			Route::prefix('{idSeccion}/menus')->name('.menus')->group(function() {
				Route::get('/', 'MenuController@vue')->name('');
				Route::post('/', 'MenuController@index')->name('');
				Route::get('crear', 'MenuController@vue')->name('.crear');
				Route::post('crear', 'MenuController@crear')->name('.crear');
				Route::get('{id}/editar', 'MenuController@vue')->name('.editar');
				Route::post('editar', 'MenuController@editar')->name('.editar');
				Route::post('guardar', 'MenuController@guardar')->name('.guardar');
				Route::post('actualizar', 'MenuController@actualizar')->name('.actualizar');
				Route::post('eliminar', 'MenuController@eliminar')->name('.eliminar');

				// ACCIONES
				Route::prefix('{idMenus}/acciones')->name('.acciones')->group(function() {
					Route::get('/', 'AccionController@vue')->name('');
					Route::post('/', 'AccionController@index')->name('');
					Route::get('crear', 'AccionController@vue')->name('.crear');
					Route::post('crear', 'AccionController@crear')->name('.crear');
					Route::get('{id}/editar', 'AccionController@vue')->name('.editar');
					Route::post('editar', 'AccionController@editar')->name('.editar');
					Route::post('guardar', 'AccionController@guardar')->name('.guardar');
					Route::post('actualizar', 'AccionController@actualizar')->name('.actualizar');
					Route::post('eliminar', 'AccionController@eliminar')->name('.eliminar');
				});
			});
		});
		// Nuestras Cuentas
		Route::prefix('cuentas')->name('.cuentas')->group(function(){
			Route::get('/', 'CuentaController@vue');
			Route::post('/', 'CuentaController@index');
			Route::post('crear', 'CuentaController@crear');
			Route::post('editar', 'CuentaController@editar');
			Route::post('desactivar', 'CuentaController@desactivar');
		});	
	});
});