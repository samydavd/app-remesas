<?php

// RUTAS SIN PERMISOS
Route::namespace('Cliente')->prefix('cliente')->name('cliente')->group(function() {

});

// RUTAS CON PERMISOS
Route::middleware('auth')->group(function() {
	Route::namespace('Cliente')->prefix('cliente')->name('cliente')->group(function() {

		// TRANSFERENCIAS
		Route::prefix('transferencias')->name('.transferencias')->group(function() {
			// LISTADO PENDIENTES
			Route::get('/', 'TransferenciaDetalleController@vue');
			Route::post('/', 'TransferenciaDetalleController@index');
			Route::post('detalles', 'TransferenciaDetalleController@detalles')->name('.detalles');
			Route::post('aceptar', 'TransferenciaDetalleController@aceptar')->name('.aceptar');
			Route::post('rechazar', 'TransferenciaDetalleController@rechazar')->name('.rechazar');
			Route::post('finalizar', 'TransferenciaDetalleController@finalizar')->name('.finalizar');
			
			Route::post('contactar_soporte', 'TransferenciaDetalleController@contactar_soporte')
			->name('.contactar_soporte');

			Route::post('regresar/verificacion', 'TransferenciaDetalleController@regresar_verificacion')
			->name('.regresar.verificacion');

			// LISTADO RECHAZADAS
			Route::prefix('rechazadas')->name('.rechazadas')->group(function() {
				Route::get('/', 'RechazoController@vue')->name('.vue');
				Route::post('/', 'RechazoController@index')->name('.index');
				Route::post('detalles', 'RechazoController@rechazos')->name('.detalles');
			});

			// LISTADO FINALIZADAS
			Route::prefix('finalizadas')->name('.finalizadas')->group(function() {
				Route::get('/', 'TransferenciaDetalleController@vue');
				Route::post('/', 'TransferenciaDetalleController@get_finalizadas');
				Route::post('soporte/reenviar', 'TransferenciaDetalleController@reenviar_soporte')->name('.reenviar_soporte');
			});
		});

	});
	
});
