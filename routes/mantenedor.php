<?php

// RUTAS SIN PERMISOS
Route::namespace('Mantenedor')->prefix('mantenedor')->name('mantenedor')->group(function() {

});

// RUTAS CON PERMISOS
Route::middleware('auth')->group(function() {
	Route::namespace('Comun')->prefix('mantenedor')->name('mantenedor')->group(function() {

		// TIPOS
		Route::prefix('tipos')->name('.tipos')->group(function() {
			Route::get('/', 'TipoController@vue');
			Route::post('/', 'TipoController@index');
			Route::post('guardar', 'TipoController@guardar')->name('.guardar');
			Route::post('eliminar', 'TipoController@eliminar')->name('.eliminar');

			// SUBTIPOS
			Route::prefix('{idTipo}/subtipos')->name('.subtipo')->group(function() {
				Route::get('/', 'SubtipoController@vue')->name('.index');
				Route::post('/', 'SubtipoController@index')->name('.index');
				Route::post('guardar', 'SubtipoController@guardar')->name('.guardar');
				Route::post('eliminar', 'SubtipoController@eliminar')->name('.eliminar');
			});
		});

		// PAISES
		Route::prefix('paises')->name('.paises')->group(function() {
			Route::get('/', 'PaisController@vue')->name('.index');
			Route::post('/', 'PaisController@index')->name('.index');
			Route::post('guardar', 'PaisController@guardar')->name('.guardar');
			Route::post('actualizar', 'PaisController@actualizar')->name('.actualizar');
			Route::post('eliminar', 'PaisController@eliminar')->name('.eliminar');
		});

		// TASAS
		Route::prefix('tasas')->name('.tasas')->group(function(){
			Route::get('/', 'TasasController@vue');
			Route::post('/', 'TasasController@index');
			Route::post('crear', 'TasasController@crearEdit')->name('.crear');
			Route::post('editar', 'TasasController@crearEdit')->name('.editar');
			Route::post('estado', 'TasasController@estado')->name('.estado');
		});

	});

	Route::namespace('Cliente')->prefix('mantenedor')->name('mantenedor')->group(function() {

		// MONEDAS
		Route::prefix('monedas')->name('.monedas')->group(function() {
			Route::get('/', 'MonedaController@vue')->name('.index');
			Route::post('/', 'MonedaController@index')->name('.index');
			Route::post('guardar', 'MonedaController@guardar')->name('.guardar');
			Route::post('actualizar', 'MonedaController@actualizar')->name('.actualizar');
			Route::post('eliminar', 'MonedaController@eliminar')->name('.eliminar');
		});

		// BANCOS
		Route::prefix('bancos')->name('.bancos')->group(function(){
			Route::get('/', 'BancoController@vue');
			Route::post('/', 'BancoController@index');
			Route::post('guardar', 'BancoController@guardar')->name('.guardar');
			Route::post('actualizar', 'BancoController@actualizar')->name('.actualizar');
			Route::post('eliminar', 'BancoController@eliminar')->name('.eliminar');
			Route::post('asignar', 'BancoController@asignar')->name('.asignar');
			Route::post('tipo/desactivar', 'BancoController@desactivarTipo')->name('.desactivar_tipo');
			Route::post('tipo/activar', 'BancoController@activarTipo')->name('.activar_tipo');
		});

	});
});
