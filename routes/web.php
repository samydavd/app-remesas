<?php

Route::get('/', function () {
   return redirect()->action('Auth\LoginController@showLoginForm');
});

// RUTAS AUTENTICACION
Route::namespace('Auth')->group(function () {
	Route::get('login', 'LoginController@showLoginForm');
	Route::post('login', 'LoginController@login')->name('login');
	Route::get('logout', 'LoginController@logout')->name('logout');    
});

//Auth::routes();

// RUTAS CON PERMISOS

Route::middleware('auth')->group(function () {
	Route::get('/home', 'HomeController@index')->name('home');
});
